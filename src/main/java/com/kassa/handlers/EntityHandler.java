package com.kassa.handlers;

import com.kassa.models.Entity;
import com.kassa.models.dtos.EntityDto;
import com.kassa.models.dtos.Error;
import com.kassa.models.dtos.Response;
import com.kassa.models.exceptions.UnsupportedMessageException;
import com.kassa.odoo.interfaces.clients.IOdooClient;
import com.kassa.rabbitmq.interfaces.ISender;
import lombok.extern.log4j.Log4j2;
import org.apache.xmlrpc.XmlRpcException;

@Log4j2
public class EntityHandler<T extends Entity> {
    private final IOdooClient<T> odooClient;
    private final ISender sender;

    /**
     * @param client The right OdooClient with the right Generic Type that needs to be injected
     */
    public EntityHandler(IOdooClient<T> client, ISender sender) {
        odooClient = client;
        this.sender = sender;
    }

    /**
     * Method that check what method needs to be invoked
     *
     * @param data The data enveloppe that needs to be handled
     */
    public void handleDataEnveloppe(EntityDto<T> data) {
        switch (data.getAction()) {
            case "create" -> create(data);
            case "update" -> update(data);
            case "delete" -> delete(data);
        }
    }

    /**
     * Retrieves the entity object, better known as the properties, out of the data enveloppe, cast that to the right
     * generic class, creates this in Odoo through the odooClient and finally sends this to the UUID master
     *
     * @param data DataEnveloppe that represents the data for Odoo and RabbitMQ
     */
    private void create(EntityDto<T> data) {
        T t = data.getProperties();
        try {
            //CREATE USER IN DATABASE
            int id = odooClient.create(t);
            data.setSourceId(id);
            sendToUuid(data, "created");
        } catch (UnsupportedMessageException e) {
            String message = e.getMessage() + ": " + e.getCause().getMessage();
            log.error(message);
            sender.sendError(Error.Level.ERROR, message);
        } catch (ClassCastException e) {
            String message = "Something went wrong while trying to create an instance of "
                    + t.getClass().getSimpleName().toLowerCase() + " in Odoo";
            log.error(message);
            sender.sendError(Error.Level.ERROR, message);
        }
    }


    /**
     * Retrieves the entity object, better known as the properties, out of the data enveloppe, cast that to the right
     * generic class, updates this in Odoo through the odooClient and finally sends this to the UUID master
     *
     * @param data DataEnveloppe that represents the data for Odoo and RabbitMQ
     */
    private void update(EntityDto<T> data) {
        T t = data.getProperties();
        try {
            t.setId(Integer.parseInt(data.getSourceId()));
            odooClient.update(t);
            sendToUuid(data, "updated");
        } catch (XmlRpcException | UnsupportedMessageException e) {
            String message = e.getMessage() + ": " + e.getCause().getMessage();
            log.error(message);
            sender.sendError(Error.Level.ERROR, message);
        }
    }

    /**
     * Retrieves the entity object, better known as the properties, out of the data enveloppe,
     * deletes this in Odoo through the odooClient and finally sends this to the UUID master
     *
     * @param data DataEnveloppe that represents the data for Odoo and RabbitMQ
     */
    private void delete(EntityDto<T> data) {
        T t = data.getProperties();
        try {
            t.setId(Integer.parseInt(data.getSourceId()));
            odooClient.archive(t.getId());
            sendToUuid(data, "deleted");
        } catch (XmlRpcException e) {
            String message = "Something went wrong while trying to archive a \""
                    + t.getClass().getSimpleName().toLowerCase() + "\" document in Odoo ";
            sender.sendError(message, e);
        }
    }

    /**
     * private method that sends a response to the uuid-manager
     *
     * @param data   the related dto where the uuid-manager needs a response from
     * @param action the action that the uuid-manager needs to know
     */
    private void sendToUuid(EntityDto<T> data, String action) {
        String entity = data.getProperties().getClass().getSimpleName().toLowerCase();
        Response response = new Response("kassa", data.getSourceId(), data.getUuid(), entity, action);
        sender.sendResponse(response);
    }
}
