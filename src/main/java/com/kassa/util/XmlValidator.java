package com.kassa.util;

import com.kassa.models.exceptions.XmlValidationException;
import lombok.extern.log4j.Log4j2;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

@Log4j2
public class XmlValidator {
    /**
     * This validates the xml according to a specified xsd
     *
     * @param fileName the name of the xsd-file <b>without</b> the xsd-extension
     * @param xml byte array that represents the xml
     * @throws XmlValidationException is thrown when the validation fails
     */
    public void validateXml(String fileName, byte[] xml) throws XmlValidationException {
        SchemaFactory factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
        try {
            Schema schema = factory.newSchema(XmlParser.class.getClassLoader().getResource("validation/" + fileName + ".xsd"));
            Validator validator = schema.newValidator();
            validator.validate(new StreamSource(new ByteArrayInputStream(xml)));
        } catch (IOException e) {
            throw new XmlValidationException("Something went wrong while validating the xml: " + e.getMessage());
        } catch (SAXException e) {
            throw new XmlValidationException("Invalid xml for " + fileName + ": " + e.getMessage());
        }
    }

    /**
     * This validates the xml according to a specified xsd
     *
     * @param fileName the name of the xsd-file <b>without</b> the xsd-extension
     * @param xml a xml string
     * @throws XmlValidationException is thrown when the validation fails
     * @see #validateXml(String, byte[])
     */
    public void validateXml(String fileName, String xml) throws XmlValidationException {
        validateXml(fileName, xml.getBytes(StandardCharsets.UTF_8));
    }
}
