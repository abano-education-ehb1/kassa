package com.kassa.util.adapters;

import jakarta.xml.bind.annotation.adapters.XmlAdapter;

import java.time.LocalDateTime;
import java.time.ZoneOffset;

public class LocalDateTimeAdapter extends XmlAdapter<Long, LocalDateTime> {
    @Override
    public LocalDateTime unmarshal(Long seconds) {
        return LocalDateTime.ofEpochSecond(seconds, 0, ZoneOffset.UTC);
    }

    @Override
    public Long marshal(LocalDateTime localDateTime) {
        return localDateTime.toEpochSecond(ZoneOffset.UTC);
    }
}
