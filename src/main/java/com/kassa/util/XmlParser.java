package com.kassa.util;

import com.kassa.models.Entity;
import com.kassa.models.dtos.EntityDto;
import com.kassa.models.dtos.Error;
import com.kassa.models.dtos.Heartbeat;
import com.kassa.models.dtos.Response;
import com.kassa.models.exceptions.UnsupportedMessageException;
import com.kassa.models.exceptions.XmlParsingException;
import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.JAXBException;
import jakarta.xml.bind.Marshaller;
import lombok.extern.log4j.Log4j2;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.stream.StreamSource;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;

@Log4j2
public class XmlParser {
    /**
     * Converts a DataEnveloppe object to an XML-string
     *
     * @param data The DataEnveloppe to be converted
     * @return XML-string that represents the data enveloppe
     * @throws JAXBException thrown by JAXB when an error occurred while making the JAXBContext or while setting a property from the marshaller
     */
    public static String dataToXml(EntityDto<? extends Entity> data) throws JAXBException {
        JAXBContext jaxbContext = JAXBContext.newInstance(data.getClass());
        Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

        //Required formatting?
        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true); // To format XML

        //Print XML String to Console
        StringWriter sw = new StringWriter();

        //Write XML to StringWriter
        jaxbMarshaller.marshal(data, sw);

        //Verify XML Content
        return sw.toString();
    }

    /**
     * Converts a heartbeat object to an XML-string
     *
     * @param heartbeat The Heartbeat to be converted
     * @return XML-string that represents the heartbeat
     * @throws JAXBException thrown by JAXB when an error occurred while making the JAXBContext or while setting a property from the marshaller
     */
    public static String heartbeatToXml(Heartbeat heartbeat) throws JAXBException {
        JAXBContext jaxbContext = JAXBContext.newInstance(Heartbeat.class);
        Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true); // To format XML

        StringWriter sw = new StringWriter();

        jaxbMarshaller.marshal(heartbeat, sw);

        return sw.toString();
    }

    public static String responseToXml(Response response) throws JAXBException {
        JAXBContext jaxbContext = JAXBContext.newInstance(Response.class);
        Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true); // To format XML

        StringWriter sw = new StringWriter();

        jaxbMarshaller.marshal(response, sw);

        return sw.toString();
    }

    public static String errorToXml(Error error) throws JAXBException {
        JAXBContext jaxbContext =  JAXBContext.newInstance(Error.class);
        Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

        StringWriter sw = new StringWriter();

        jaxbMarshaller.marshal(error, sw);

        return sw.toString();
    }


    /**
     * Converts an array of bytes that represents the XML-string to an DataEnveloppe object
     *
     * @param cls The class whereto the data should be parsed
     * @param data The array of bytes to be converted
     * @return The converted DataEnveloppe object
     */
    public static <T extends EntityDto<? extends Entity>> T xmlToData(byte[] data, Class<T> cls) throws JAXBException {
        JAXBContext jaxbContext = JAXBContext.newInstance(cls);
        return jaxbContext.createUnmarshaller().unmarshal(new StreamSource(new ByteArrayInputStream(data)), cls).getValue();
    }

    /**
     * Retrieves info about the entity and the action out of the xml
     *
     * @param data byte array that represents the xml
     * @return a Map where the values store the information about the key
     */
    public static Map<String, String> getInfoFromXml(byte[] data) throws XmlParsingException {
        try {
            Map<String, String> docMap = new HashMap<>();
            DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance(); // create a factory for creating a builder
            DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder(); // create a builder for creating a document
            Document document = documentBuilder.parse(new ByteArrayInputStream(data)); // create a document based on the byte array
            document.getDocumentElement().normalize();

            docMap.put("entity", document.getElementsByTagName("entity").item(0).getTextContent());
            docMap.put("action", document.getElementsByTagName("action").item(0).getTextContent());

            return docMap;
        } catch (ParserConfigurationException | SAXException | IOException e) {
//            String message = "Something went wrong while trying to read the incoming XML";
//            sender.sendError(message, e);
            throw new XmlParsingException("Something went wrong while trying to read the incoming XML", e);
        }
    }

    public static String getRootElementTagName(byte[] data) throws UnsupportedMessageException, XmlParsingException {
        String entity = null;
        try {
            DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance(); // create a factory for creating a builder
            DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder(); // create a builder for creating a document
            Document document = documentBuilder.parse(new ByteArrayInputStream(data)); // create a document based on the byte array
            document.getDocumentElement().normalize();

            entity = document.getDocumentElement().getNodeName();
        } catch (ParserConfigurationException | IOException | IllegalArgumentException e) {
//            String message = "Something went wrong while trying to read the incoming XML";
//            log.error(message);
//            sender.sendError(Error.Level.ERROR, message);
            throw new XmlParsingException("Something went wrong while trying to read the incoming XML", e);
        } catch (SAXException e) {
            throw new UnsupportedMessageException("The message has an unsupported entity");
        }

        return entity;
    }

}
