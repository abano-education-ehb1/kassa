package com.kassa.util;

import com.kassa.models.exceptions.CredentialsMissingException;
import lombok.extern.log4j.Log4j2;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

@Log4j2
public class CredentialsLoader {
    private static Properties credentials;
    private static final List<String> missingEnvs = new ArrayList<>(); // Static List that helps to foresee the error sufficient info
    // all the required envs
    private static final Map<String, EnvType> envs = new HashMap<>() {{
        put("ODOO_HOST", EnvType.PLAIN_TEXT);
        put("ODOO_DB", EnvType.PLAIN_TEXT);
        put("ODOO_USERNAME", EnvType.PLAIN_TEXT);
        put("RABBITMQ_USERNAME", EnvType.PLAIN_TEXT);
        put("RABBITMQ_HOSTNAME", EnvType.PLAIN_TEXT);
        put("ODOO_KEY", EnvType.SECRET);
        put("RABBITMQ_PASSWORD", EnvType.SECRET);
    }};


    /**
     * The credentials are retrieved from the environment variables and returned as a Properties object
     *
     * @return Properties that stores the necessary credentials
     */
    public static Properties getCredentials() throws CredentialsMissingException {
        try {
            if (credentials == null) {
                credentials = new Properties();

                for(String env : envs.keySet()) {
                    String value = switch (envs.get(env)) {
                        case PLAIN_TEXT -> getEnv(env);
                        case SECRET -> getSecret(env);
                    };
                    String key = env.replace('_', '.').toLowerCase(Locale.ROOT);
                    credentials.setProperty(key, value);
                }
            }
        } catch (NullPointerException e) { // is thrown when a env is missing
            throw new CredentialsMissingException(missingEnvs.toArray(new String[0]));
        }
        return credentials;
    }

    private static String getEnv(String env) {
        String value = System.getenv(env);
        if(value == null) {
            missingEnvs.add(env);
        }
        return value;
    }

    private static String getSecret(String env) { // env can have for example the value ODOO_KEY
        String secret = System.getenv(env); // sees first if the secret is saved as an environment variable
        if (secret == null) { // if that secret is null, go load the secret out of the specified file
            try {
                String filename = System.getenv(env + "_FILE"); // search in the according file
                if (filename == null) {
                    missingEnvs.add(env); // if the file does not exist, prepare this class for throwing an exception with sufficient info.
                } else {
                    BufferedReader bufferedReader = new BufferedReader(new FileReader(filename));
                    secret = bufferedReader.readLine();
                }
            } catch (IOException e) {
                missingEnvs.add(env);
            }
        }
        return secret;
    }

    private enum EnvType {
        PLAIN_TEXT,
        SECRET
    }
}
