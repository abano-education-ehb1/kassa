package com.kassa.rabbitmq;

import com.kassa.models.Entity;
import com.kassa.models.dtos.EntityDto;
import com.kassa.models.dtos.Error;
import com.kassa.models.dtos.Heartbeat;
import com.kassa.models.dtos.Response;
import com.kassa.models.exceptions.XmlValidationException;
import com.kassa.rabbitmq.interfaces.ISender;
import com.kassa.util.CredentialsLoader;
import com.kassa.util.XmlParser;
import com.kassa.util.XmlValidator;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import jakarta.xml.bind.JAXBException;
import lombok.extern.log4j.Log4j2;

import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeoutException;

@Log4j2
public class Sender implements ISender {
    private final ConnectionFactory factory;
    private final XmlValidator validator;

    public Sender() {
        Properties credentials = CredentialsLoader.getCredentials();
        factory = new ConnectionFactory();
        factory.setHost(credentials.getProperty("rabbitmq.hostname"));
        factory.setUsername(credentials.getProperty("rabbitmq.username"));
        factory.setPassword(credentials.getProperty("rabbitmq.password"));

        validator = new XmlValidator();
    }

    @Override
    public <T extends Entity> boolean sendData(EntityDto<T> data, String entity) {
        try (Connection connection = factory.newConnection();
             Channel channel = connection.createChannel()) {
            channel.queueDeclare(RabbitMqNames.UUID_QUEUE_NAME, false, false, false, null);

            String message = XmlParser.dataToXml(data);

            validator.validateXml(entity, message);

            channel.basicPublish("", RabbitMqNames.UUID_QUEUE_NAME, null, message.getBytes());
            log.info("A new message is sent - " + entity + ":" + data.getAction());

            return true;
        } catch (IOException | TimeoutException e) {
            String message = "Failed to send " + entity + ":" + data.getAction() + " message" + e.getMessage();
            log.fatal(message);
            log.fatal(e.getMessage());
        } catch (JAXBException | XmlValidationException e) {
            String message = "Failed to send " + entity + ":" + data.getAction() + " message" + e.getMessage();
            log.error(message);
            log.error(e.getMessage());
            Error error = new Error(Error.Level.FATAL, message + ":" + e.getMessage());
            sendError(error);
        }

        return false;
    }

    @Override
    public void sendResponse(Response response) {
        try (Connection connection = factory.newConnection();
             Channel channel = connection.createChannel()) {
            channel.queueDeclare(RabbitMqNames.UUID_QUEUE_NAME, false, false, false, null);

            String message = XmlParser.responseToXml(response);

            validator.validateXml("response", message);

            channel.basicPublish("", RabbitMqNames.UUID_QUEUE_NAME, null, message.getBytes());
            log.info("A new message is sent - " + response.getEntity() + ":" + response.getAction());
        } catch (IOException | TimeoutException e) {
            String message = "Failed to send a response to UUID " + response.getEntity() + ":" + response.getAction() + ": " + e.getMessage();
            log.fatal(message);
            log.fatal(e.getMessage());
        } catch (JAXBException | XmlValidationException e) {
            String message = "Failed to send a response to UUID " + response.getEntity() + ":" + response.getAction() + ": " + e.getMessage();
            Error error = new Error(Error.Level.FATAL, message);
            sendError(error);
            log.error(message);
            log.error(e.getMessage());
        }
    }

    @Override
    public void sendHeartbeat(Heartbeat heartbeat) {
        try (Connection connection = factory.newConnection();
             Channel channel = connection.createChannel()) {
            XmlValidator validator = new XmlValidator();
            String message = XmlParser.heartbeatToXml(heartbeat);
            validator.validateXml("heartbeat", message);
            channel.basicPublish("", RabbitMqNames.MONITORING_QUEUE_NAME, null, message.getBytes());
        } catch (IOException | TimeoutException e) {
            log.fatal("Failed to send heartbeat due to connection problems");
//            log.fatal(e.getMessage());
        } catch (JAXBException | XmlValidationException e) {
            log.fatal("Failed to send heartbeat due to invalid format");
            log.fatal(e.getMessage());
            sendError(Error.Level.FATAL, "Failed to send heartbeat due to invalid format");
        }
    }

    @Override
    public void sendError(Error error) {
        try (Connection connection = factory.newConnection();
             Channel channel = connection.createChannel()) {
            XmlValidator validator = new XmlValidator();
            String message = XmlParser.errorToXml(error);
            validator.validateXml("error", message);
            channel.basicPublish("", RabbitMqNames.ERROR_QUEUE_NAME, null, message.getBytes());
        } catch (IOException | JAXBException | TimeoutException | XmlValidationException e) {
            log.fatal("Failed to send error message \"" + error.getMessage() + "\"");
            log.error(e.getMessage());
        }
    }

    @Override
    public void sendError(Error.Level errorLevel, String message) {
        sendError(new Error(errorLevel, message));
    }

    @Override
    public void sendError(Error.Level errorLevel, Exception e) {
        sendError(new Error(errorLevel, e.getMessage()));
    }

    @Override
    public void sendError(String message, Exception e) {
        log.error(message);
        log.error(e.getMessage());
        sendError(Error.Level.ERROR, message + ": " + e.getMessage());
    }

}
