package com.kassa.rabbitmq;

public class RabbitMqNames {
    public static final String UUID_QUEUE_NAME = "uuid_manager";
    public static final String MONITORING_QUEUE_NAME = "monitoring";
    public static final String MONITORING_ROUTING_KEY = "monitoring.reporting";
    public static final String UUID_EXCHANGE_NAME = "uuid_manager_exchange";
    public static final String KASSA_QUEUE_NAME = "kassa";
    public static final String ERROR_QUEUE_NAME = "errors";
}
