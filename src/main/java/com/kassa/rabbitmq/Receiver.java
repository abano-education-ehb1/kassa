package com.kassa.rabbitmq;

import com.kassa.handlers.EntityHandler;
import com.kassa.models.Company;
import com.kassa.models.Order;
import com.kassa.models.User;
import com.kassa.models.dtos.CompanyDto;
import com.kassa.models.dtos.Error;
import com.kassa.models.dtos.OrderDto;
import com.kassa.models.dtos.UserDto;
import com.kassa.models.exceptions.UnsupportedMessageException;
import com.kassa.models.exceptions.XmlParsingException;
import com.kassa.models.exceptions.XmlValidationException;
import com.kassa.odoo.clients.CompanyClient;
import com.kassa.odoo.clients.OrderClient;
import com.kassa.odoo.clients.UserClient;
import com.kassa.rabbitmq.interfaces.ISender;
import com.kassa.util.CredentialsLoader;
import com.kassa.util.XmlParser;
import com.kassa.util.XmlValidator;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.DeliverCallback;
import jakarta.xml.bind.JAXBException;
import lombok.extern.log4j.Log4j2;

import java.util.Properties;

@Log4j2
public class Receiver {
    private final ISender sender;

    public Receiver(ISender sender) {
        this.sender = sender;
    }

    public void receive() throws Exception {
        final Properties credentials = CredentialsLoader.getCredentials();

        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost(credentials.getProperty("rabbitmq.hostname"));
        factory.setUsername(credentials.getProperty("rabbitmq.username"));
        factory.setPassword(credentials.getProperty("rabbitmq.password"));
        factory.setAutomaticRecoveryEnabled(true);

        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();

        channel.queueDeclare(RabbitMqNames.KASSA_QUEUE_NAME, false, false, false, null);

        log.info("System is now ready to receive messages from RabbitMQ");

        DeliverCallback deliverCallback = (consumerTag, delivery) -> {
            try {
                byte[] data = delivery.getBody();
                XmlValidator validator = new XmlValidator();

                String entity = XmlParser.getRootElementTagName(data);

                validator.validateXml(entity, data);

                handleIncomingXml(entity, data);
            } catch (XmlValidationException | UnsupportedMessageException | XmlParsingException e) {
                String message = "Received an invalid xml: " + e.getMessage();
                log.error(message);
//                log.error(new String(delivery.getBody()));
                sender.sendError(Error.Level.ERROR, message);
            }
        };
        channel.basicConsume(RabbitMqNames.KASSA_QUEUE_NAME, true, deliverCallback, consumerTag -> {
        });
    }

    /**
     * Private method that first checks whether xml is sent by <i>kassa</i> or not and then looks from what entity
     * the properties are, so that the right handler could handle the dataEnveloppe
     *
     * @param data The dataEnveloppe that needs to be checked and to be sent to the right handler
     */
    private void handleIncomingXml(String entity, byte[] data) {
        try {
            switch (entity) {
                case "user" -> {
                    EntityHandler<User> handler = new EntityHandler<>(new UserClient(), sender);
                    UserDto dto = XmlParser.xmlToData(data, UserDto.class);
                    handler.handleDataEnveloppe(dto);
                }
                case "company" -> {
                    EntityHandler<Company> handler = new EntityHandler<>(new CompanyClient(), sender);
                    CompanyDto dto = XmlParser.xmlToData(data, CompanyDto.class);
                    handler.handleDataEnveloppe(dto);
                }
                case "order" -> {
                    EntityHandler<Order> handler = new EntityHandler<>(new OrderClient(), sender);
                    OrderDto dto = XmlParser.xmlToData(data, OrderDto.class);
                    handler.handleDataEnveloppe(dto);
                }
                default -> {
                }

            }
        } catch (JAXBException e) {
            String message = "Unable to use the incoming xml";
            sender.sendError(message, e);
        }
    }

}
