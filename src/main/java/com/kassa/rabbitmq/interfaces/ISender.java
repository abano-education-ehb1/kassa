package com.kassa.rabbitmq.interfaces;

import com.kassa.models.Entity;
import com.kassa.models.dtos.EntityDto;
import com.kassa.models.dtos.Error;
import com.kassa.models.dtos.Heartbeat;
import com.kassa.models.dtos.Response;

public interface ISender {
    <T extends Entity> boolean sendData(EntityDto<T> data, String entity);

    void sendResponse(Response response);

    void sendHeartbeat(Heartbeat heartbeat);

    void sendError(Error error);

    void sendError(Error.Level errorLevel, String message);

    void sendError(Error.Level errorLevel, Exception e);

    void sendError(String message, Exception e);
}
