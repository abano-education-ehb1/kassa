package com.kassa.rabbitmq;

import com.kassa.models.dtos.Heartbeat;
import com.kassa.rabbitmq.interfaces.ISender;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class HeartbeatScheduler {
    private final Runnable heartbeatRunnable;

    public HeartbeatScheduler(ISender sender) {

        //https://stackoverflow.com/questions/409932/java-timer-vs-executorservice
        //https://stackoverflow.com/questions/12908412/print-hello-world-every-x-seconds
        heartbeatRunnable = () -> {
            Heartbeat heartbeat = new Heartbeat();
            sender.sendHeartbeat(heartbeat);
        };
    }

    public void startHeartbeat() {
        ScheduledExecutorService executor = Executors.newScheduledThreadPool(1);
        executor.scheduleAtFixedRate(heartbeatRunnable, 0, 3, TimeUnit.SECONDS);
    }
}
