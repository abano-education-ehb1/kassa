package com.kassa.models;

public interface Entity {
    int getId();
    void setId(int id);
}