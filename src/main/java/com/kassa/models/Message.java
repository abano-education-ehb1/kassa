package com.kassa.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class Message implements Entity {
    private int id;
    private String action;
    private String entity;
    private int relatedId;
    private Entity relatedDocument;

    @Override
    public String toString() {
        return "Message{" +
                "id=" + id +
                ", action='" + action + '\'' +
                ", entity='" + entity + '\'' +
                ", relatedId=" + relatedId +
                '}';
    }
}
