package com.kassa.models.dtos;

import com.kassa.models.Order;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "order")
@XmlAccessorType(XmlAccessType.FIELD)
public class OrderDto extends EntityDto<Order> {
    @XmlElement(name = "properties")
    private Order order;
    @Override
    public Order getProperties() {
        return order;
    }

    @Override
    public void setProperties(Order order) {
        this.order = order;
    }
}
