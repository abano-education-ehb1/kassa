package com.kassa.models.dtos;

import jakarta.xml.bind.annotation.*;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.time.ZoneOffset;


@Data
@NoArgsConstructor
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = {
        "source",
        "date",
        "level",
        "message"
})
@XmlRootElement(name = "error")
public final class Error {
    @XmlEnum
    public enum Level {
        @XmlEnumValue("warn")
        WARN,
        @XmlEnumValue("error")
        ERROR,
        @XmlEnumValue("fatal")
        FATAL
    }
    private String source;
    private String date;
    private Level level;
    private String message;

    public Error(Level level, String message) {
        source = "kassa";
        date = String.valueOf(LocalDateTime.now().toEpochSecond(ZoneOffset.UTC));
        this.level = level;
        this.message = message;
    }

}
