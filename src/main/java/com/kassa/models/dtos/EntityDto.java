package com.kassa.models.dtos;

import com.kassa.models.Entity;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@XmlAccessorType(XmlAccessType.FIELD)
public abstract class EntityDto<T extends Entity> {
    @XmlElement
    private String source;
    @XmlElement(name = "source-id")
    private String sourceId;
    @XmlElement
    private String uuid;
    @XmlElement
    private String action;

    public void setSourceId(String sourceId) {
        this.sourceId = sourceId;
    }

    public void setSourceId(int id) {
        sourceId = String.valueOf(id);
    }

    public abstract T getProperties();
    public abstract void setProperties(T t);
}
