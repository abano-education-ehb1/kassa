package com.kassa.models.dtos;

import com.kassa.models.User;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "user")
@XmlAccessorType(XmlAccessType.FIELD)
public class UserDto extends EntityDto<User> {
    @XmlElement(name = "properties")
    private User user;

    @Override
    public User getProperties() {
        return user;
    }

    @Override
    public void setProperties(User user) {
        this.user = user;
    }
}
