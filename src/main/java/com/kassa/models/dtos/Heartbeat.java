package com.kassa.models.dtos;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlRootElement;
import jakarta.xml.bind.annotation.XmlType;

import java.time.LocalDateTime;
import java.time.ZoneOffset;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = {
        "source",
        "date"
})
@XmlRootElement(name = "heartbeat")
public final class Heartbeat {
    private final String source;
    private final String date;

    public Heartbeat() {
        source = "kassa";
        date = String.valueOf(LocalDateTime.now().toEpochSecond(ZoneOffset.UTC));
    }

    public String getSource() {
        return source;
    }

    public String getDate() {
        return date;
    }
}
