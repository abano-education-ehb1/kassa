package com.kassa.models.dtos;


import com.kassa.models.Company;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "company")
@XmlAccessorType(XmlAccessType.FIELD)
public class CompanyDto extends EntityDto<Company> {
    @XmlElement(name = "properties")
    private Company company;

    @Override
    public Company getProperties() {
        return company;
    }

    @Override
    public void setProperties(Company company) {
        this.company = company;
    }
}
