package com.kassa.models.dtos;

import com.kassa.models.Entity;
import jakarta.xml.bind.annotation.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Locale;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement
@XmlType(propOrder = {
        "source",
        "sourceId",
        "uuid",
        "entity",
        "action"
})
@Data
@NoArgsConstructor
@AllArgsConstructor
public final class Response {
    @XmlElement
    private String source;
    @XmlElement(name = "source-id")
    private String sourceId;
    @XmlElement
    private String uuid;
    private String entity;
    @XmlElement
    private String action;

    public <T extends Entity> Response(EntityDto<T> dto) {
        source = "kassa";
        sourceId = dto.getSourceId();
        uuid = dto.getUuid();
        entity = dto.getProperties().getClass().getSimpleName().toLowerCase(Locale.ROOT);
        action = dto.getAction() + "d";
    }
}
