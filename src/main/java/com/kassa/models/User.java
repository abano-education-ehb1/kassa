package com.kassa.models;

import jakarta.xml.bind.annotation.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "properties")
@XmlType(propOrder = {
        "firstname",
        "lastname",
        "email",
        "address",
        "phone"
})

@NoArgsConstructor
@AllArgsConstructor
@Data
public class User implements Entity {
    @XmlTransient
    private int id;
    @XmlElement
    private String firstname;
    @XmlElement
    private String lastname;
    @XmlElement(name = "email")
    private String email;
    @XmlElement(name = "address")
    private Address address;
    @XmlElement(name = "phone")
    private String phone;

    public String getFullName() {
        return firstname + " " + lastname;
    }

    public void setFullName(String name) {
        name = name.trim();
        firstname = name.substring(0, name.indexOf(" "));
        lastname = name.substring(name.indexOf(" ") + 1);
    }
}
