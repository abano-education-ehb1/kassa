package com.kassa.models;

import jakarta.xml.bind.annotation.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "properties")
@XmlType(propOrder = {
        "name",
        "email",
        "address",
        "phone",
        "taxId"
})

@AllArgsConstructor
@NoArgsConstructor
@Data
public class Company implements Entity {
    @XmlTransient
    private int id;
    private String name;
    private Address address;
    private String taxId;
    private String phone;
    private String email;
}
