package com.kassa.models;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = {
        "sourceId",
        "uuid"
})
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Identifiers {
    @XmlElement
    private String uuid;
    @XmlElement(name = "source-id")
    private String sourceId;

    public Identifiers(String sourceId) {
        this.sourceId = sourceId;
    }

    public Identifiers(int sourceId) {
        this.sourceId = String.valueOf(sourceId);
    }
}