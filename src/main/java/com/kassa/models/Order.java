package com.kassa.models;

import com.kassa.util.adapters.LocalDateTimeAdapter;
import jakarta.xml.bind.annotation.*;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.List;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "properties")
@XmlType(propOrder = {
        "date",
        "total",
        "userIds",
        "toInvoice",
        "orderLines",
        "isPaid"
})
@NoArgsConstructor
@AllArgsConstructor
@Data
public class Order implements Entity {
    @XmlTransient
    private int id;
    @XmlElement
    //om naar string om te zetten (geen object)
    @XmlJavaTypeAdapter(value = LocalDateTimeAdapter.class)
    private LocalDateTime date;
    @XmlElement(name = "invoice")
    private boolean toInvoice;
    @XmlElement(name = "is-paid")
    private boolean isPaid;
    @XmlElement
    private double total;
    @XmlElement(name = "user")
    private Identifiers userIds;

    public void setUser(User user) {
        this.userIds = new Identifiers(user.getId());
    }
    public void setUserId(int id) {
        this.userIds = new Identifiers(id);
    }

    public int getUserId() {
        return Integer.parseInt(userIds.getSourceId());
    }

    @XmlElementWrapper(name = "lines")
    @XmlElement(name = "line")
    private List<OrderLine> orderLines;
}
