package com.kassa.models;

import jakarta.xml.bind.annotation.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "properties")
@XmlType(propOrder = {"name", "salesPrice", "categoryIds"})

@NoArgsConstructor
@AllArgsConstructor
@Data
public class Product implements Entity {
    @XmlTransient
    private int id;
    @XmlElement
    private String name;
    @XmlElement(name = "price")
    private double salesPrice;
    @XmlTransient
    private double customerTaxes;
    @XmlTransient
    private Category category;

    @XmlElement(name = "category-identifiers")
    public Identifiers getCategoryIds() {
        return new Identifiers(category.getId());
    }
}
