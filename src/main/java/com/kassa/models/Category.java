package com.kassa.models;

import jakarta.xml.bind.annotation.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "properties")
@XmlType(propOrder = {"name"})
@NoArgsConstructor
@AllArgsConstructor
@Data
public class Category implements Entity {
    @XmlTransient
    private int id;
    @XmlElement(name = "title")
    private String name;
    @XmlTransient
    private int parentId;

    /**
     * @return id of the parent, -1 if there is no parent or 0 if parent is not set (so parentId could still be set to a valid id or -1)
     */
    public int getParentId() {
        return parentId;
    }
}
