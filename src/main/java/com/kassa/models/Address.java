package com.kassa.models;

import jakarta.xml.bind.annotation.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "street",
        "number",
        "zip",
        "city",
        "country"
})
@AllArgsConstructor
@Data
public class Address {
    @XmlElement(name = "street")
    private String street;

    @XmlElement(name = "housenumber")
    private String number;

    @XmlTransient
    private String addressLine2;

    @XmlElement(name = "city")
    private String city;

    @XmlElement(name = "postalcode")
    private String zip;
    @XmlTransient
    private Country country;

    @XmlElement(name = "country")
    public String getCountry() {
        return country.getName();
    }

    @XmlTransient
    public int getCountryId() {
        return country.getId();
    }

    public void setCountryId(int id) {
        country.setId(id);
    }

    public void setCountry(String name) {
        country.setName(name);
    }
    
    public Address() {
        country = new Country();
    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Country implements Entity {
        private int id;
        private String name;
        
        public Country(String name) {
            this.name = name;
        }
    }
}
