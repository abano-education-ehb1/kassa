package com.kassa.models.exceptions;

public class XmlValidationException extends Exception {
    public XmlValidationException() {

    }

    public XmlValidationException(String message) {
        super(message);
    }

}
