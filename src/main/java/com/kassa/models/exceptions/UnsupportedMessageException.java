package com.kassa.models.exceptions;

public class UnsupportedMessageException extends Exception {
    public UnsupportedMessageException(String message) {
        super(message);
    }

    public UnsupportedMessageException(String message, Throwable cause) {
        super(message, cause);
    }

    @Override
    public int hashCode() {
        return this.getMessage().hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if(obj == this) return true;
        if(!(obj instanceof UnsupportedMessageException)) return false;
        UnsupportedMessageException e = (UnsupportedMessageException) obj;
        return e.getMessage().equals(getMessage());
    }
}
