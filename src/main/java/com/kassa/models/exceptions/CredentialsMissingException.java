package com.kassa.models.exceptions;

public class CredentialsMissingException extends RuntimeException {
    private final String[] missingKeys;
    public CredentialsMissingException() {
        super("Not all required credentials are provided");
        missingKeys = null;
    }

    public CredentialsMissingException(String... missingKeys) {
        super("Credentials are missing for the following keys: " + String.join(", ", missingKeys));
        this.missingKeys = missingKeys;
    }

    public String[] getMissingKeys() {
        return missingKeys;
    }
}
