package com.kassa.models.exceptions;

public class XmlParsingException extends Exception {
    public XmlParsingException() {
    }

    public XmlParsingException(String message) {
        super(message);
    }

    public XmlParsingException(String message, Throwable cause) {
        super(message, cause);
    }

    public XmlParsingException(Throwable cause) {
        super(cause);
    }
}
