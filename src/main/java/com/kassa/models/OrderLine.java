package com.kassa.models;

import jakarta.xml.bind.annotation.*;
import lombok.*;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = {
        "productName",
        "unityPrice",
        "quantity"
})

@NoArgsConstructor
@AllArgsConstructor
@Data
public class OrderLine implements Entity {
    @XmlTransient
    private int id;
    private int quantity;
    @XmlElement(name = "unity-price")
    private double unityPrice;
    @XmlElement(name = "product-name")
    private String productName;


}
