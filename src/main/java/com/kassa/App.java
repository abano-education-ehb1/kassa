package com.kassa;

import com.kassa.odoo.OdooListener;
import com.kassa.odoo.clients.MessageClient;
import com.kassa.rabbitmq.HeartbeatScheduler;
import com.kassa.rabbitmq.Receiver;
import com.kassa.rabbitmq.Sender;
import com.kassa.rabbitmq.interfaces.ISender;
import lombok.extern.log4j.Log4j2;

@Log4j2
public class App {
    public static void main(String[] args) {
        ISender sender = new Sender();
        try {
            Receiver receiver = new Receiver(sender);
            receiver.receive();

            HeartbeatScheduler heartbeatScheduler = new HeartbeatScheduler(sender);
            heartbeatScheduler.startHeartbeat();

            OdooListener odooListener = new OdooListener(new MessageClient(), sender);
            odooListener.listen();

        } catch (Exception e) {
            String message = "Something went wrong while trying to initialize the system";
            log.warn("Please try to restart the system!");
            sender.sendError(message, e);
            log.info("Shutting down the system ...");
            System.exit(16); //https://www.ibm.com/docs/en/zvse/6.2?topic=SSB27H_6.2.0/fa2mu_rcf_return_code_convent.html
        }
    }
}
