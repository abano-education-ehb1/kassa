package com.kassa.odoo.transformaters;

import com.kassa.models.Category;
import com.kassa.models.Product;

import java.util.HashMap;

public class ProductTransformator implements Transformator<Product> {
    @Override
    public HashMap<String, Object> transformToMap(Product product) {
        return new HashMap<>() {{
            put("name", product.getName());
            put("list_price", product.getSalesPrice());
            put("pos_categ_id", product.getCategory().getId());
        }};
    }

    @Override
    public Product transformToObject(HashMap<String, Object> map) {
        Product product = new Product();

        // if it is not an instance of Object[], there is no category linked to the product
        if(map.get("pos_categ_id") instanceof Object[] temp) {
            Category category = new Category();
            category.setId((int) temp[0]);
            String name = (String) temp[1];
            if(name.contains("/")) {
                name = name.substring(name.lastIndexOf("/") + 2);
            }
            category.setName(name);
            product.setCategory(category);
        }

        product.setId((int) map.get("id"));
        product.setName((String) map.get("name"));
        product.setSalesPrice((Double) map.get("list_price"));
        return product;
    }
}
