package com.kassa.odoo.transformaters;

import com.kassa.models.OrderLine;

import java.util.HashMap;

public class OrderLineTransformator implements Transformator<OrderLine> {
    @Override
    public HashMap<String, Object> transformToMap(OrderLine orderLine) {
        throw new UnsupportedOperationException("Transforming a message to a hashmap is not supported");
    }

    @Override
    public OrderLine transformToObject(HashMap<String, Object> map) {
        OrderLine line = new OrderLine();
        line.setId((Integer) map.get("id"));

        if(map.get("product_id") instanceof Object[] product) {
            line.setProductName((String) product[1]);
        }
        double qty = (double) map.get("qty");
        line.setQuantity((int) qty);
        line.setUnityPrice((double) map.get("price_unit"));

        return line;
    }
}
