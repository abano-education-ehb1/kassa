package com.kassa.odoo.transformaters;

import com.kassa.models.Order;
import com.kassa.models.OrderLine;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class OrderTransformator implements Transformator<Order> {
    private final DateTimeFormatter dateTimeFormatter;
    private final ZoneId UTC = ZoneId.of("UTC");
    private final ZoneId EUROPE_BRUSSELS = ZoneId.of("Europe/Brussels");

    public OrderTransformator() {
        dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
    }

    @Override
    public HashMap<String, Object> transformToMap(Order order) {
        HashMap<String, Object> map = new HashMap<>();
        map.put("id", order.getId());
        /* First, the LocalDateTime is transformed to a ZonedDateTime with our time zone,
         * and then it's formatted to a Sting in the UTC time zone
         */
        map.put("date_order", order.getDate().atZone(EUROPE_BRUSSELS).format(dateTimeFormatter.withZone(UTC)));
        map.put("amount_total", order.getTotal());
        map.put("partner_id", order.getUserId());
        map.put("to_invoice", order.isToInvoice());
        List<Integer> orderLineIds = order.getOrderLines().stream().map(OrderLine::getId).toList();
        map.put("lines", orderLineIds);

        return map;
    }

    // Source about timezones: https://stackoverflow.com/questions/34626382/convert-localdatetime-to-localdatetime-in-utc
    @Override
    public Order transformToObject(HashMap<String, Object> map) {
        Order order = new Order();

        order.setId((Integer) map.get("id"));

        /* First, the string from the map is parsed to a LocalDateTime, secondly, the UTC timezone is assigned to it
         * and as last, the ZonedDateTime is transformed to a ZonedDateTime from our time zone and lastly,
         * it's transformed to a LocalDateTime
         */
        LocalDateTime orderDate = LocalDateTime
                .parse((String) map.get("date_order"), dateTimeFormatter)
                .atZone(UTC)
                .withZoneSameInstant(EUROPE_BRUSSELS)
                .toLocalDateTime();
        order.setDate(orderDate);

        order.setTotal((Double) map.get("amount_total"));

        Object[] user = (Object[]) map.get("partner_id");
        order.setUserId((Integer) user[0]);

        order.setToInvoice((boolean) map.get("to_invoice"));
        List<OrderLine> orderLines = Stream.of((Object[]) map.get("lines"))
                .map(id -> {
                    OrderLine line = new OrderLine();
                    line.setId((Integer) id);
                    return line;
                })
                .collect(Collectors.toList());
        order.setOrderLines(orderLines);

        return order;
    }
}
