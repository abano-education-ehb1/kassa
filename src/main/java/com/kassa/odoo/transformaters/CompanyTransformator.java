package com.kassa.odoo.transformaters;

import com.kassa.models.Address;
import com.kassa.models.Company;

import java.util.HashMap;

public class CompanyTransformator implements Transformator<Company> {
    @Override
    public HashMap<String, Object> transformToMap(Company company) {
        HashMap<String, Object> map = new HashMap<>();
        map.put("name", company.getName());
        map.put("email", company.getEmail());
        String addressLine1 = company.getAddress().getStreet() + " " + company.getAddress().getNumber();
        map.put("street", addressLine1);
        map.put("city", company.getAddress().getCity());
        map.put("zip", company.getAddress().getZip());
        map.put("country_id", company.getAddress().getCountryId());
        map.put("phone", company.getPhone().replaceAll("\\s", ""));
        map.put("vat", company.getTaxId());
        map.put("is_company", true);
        return map;
    }

    @Override
    public Company transformToObject(HashMap<String, Object> map) {
        Company company = new Company();
        Address address = new Address();
        company.setId((int) map.get("id"));
        company.setName((String) map.get("name"));
        if (map.get("email") instanceof String email)
            company.setEmail(email);
        if (map.get("phone") instanceof String phone)
            company.setPhone(phone.replaceAll("\\s", ""));
        if (map.get("vat") instanceof String vat)
            company.setTaxId(vat);
        if(map.get("street") instanceof String addressLine1) {
            addressLine1 = addressLine1.trim();
            int index = addressLine1.lastIndexOf(" ");
            if(index > 0) {
                address.setStreet(addressLine1.substring(0, index));
                address.setNumber(addressLine1.substring(index + 1));
            }
        }
        address.setCity((String) map.get("city"));
        address.setZip((String) map.get("zip"));
        if (map.get("country_id") instanceof Object[] country) {
            address.setCountry((String) country[1]);
            address.setCountryId((int) country[0]);
        }
        company.setAddress(address);
        return company;
    }
}
