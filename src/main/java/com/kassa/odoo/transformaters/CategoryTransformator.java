package com.kassa.odoo.transformaters;

import com.kassa.models.Category;

import java.util.HashMap;

public class CategoryTransformator implements Transformator<Category> {
    @Override
    public HashMap<String, Object> transformToMap(Category category) {
        return new HashMap<>(){{
           put("name", category.getName());
           put("parent_id", category.getParentId());
        }};
    }

    @Override
    public Category transformToObject(HashMap<String, Object> map) {
        Category category = new Category();
        category.setId((int) map.get("id"));
        category.setName((String) map.get("name"));
        if(map.get("parent_id") instanceof Object[]) {
            category.setParentId((int) ((Object[]) map.get("parent_id"))[0]);
        } else {
            category.setParentId(-1);
        }
        return category;
    }
}
