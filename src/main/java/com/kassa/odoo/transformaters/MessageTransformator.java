package com.kassa.odoo.transformaters;

import com.kassa.models.Message;

import java.util.HashMap;

public class MessageTransformator implements Transformator<Message> {
    @Override
    public HashMap<String, Object> transformToMap(Message message) {
        throw new UnsupportedOperationException("Transforming a message to a hashmap is not supported");
    }

    @Override
    public Message transformToObject(HashMap<String, Object> map) {
        return new Message(){{
            setId((int) map.get("id"));
            setRelatedId((int) map.get("x_rel_id"));
            setEntity((String) map.get("x_entity"));
            setAction((String) map.get("x_action"));
        }};
    }
}
