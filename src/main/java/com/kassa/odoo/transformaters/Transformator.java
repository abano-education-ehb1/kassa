package com.kassa.odoo.transformaters;

import com.kassa.models.Entity;

import java.util.HashMap;


public interface Transformator<T extends Entity> {
    HashMap<String, Object> transformToMap(T t);

    T transformToObject(HashMap<String, Object> map);
}
