package com.kassa.odoo.transformaters;

import com.kassa.models.Address;
import com.kassa.models.User;

import java.util.HashMap;

public class UserTransformator implements Transformator<User> {
    @Override
    public HashMap<String, Object> transformToMap(User user) {
        return new HashMap<>() {{
            put("name", user.getFullName());
            put("email", user.getEmail());
            String addressLine1 = user.getAddress().getStreet() + " " + user.getAddress().getNumber();
            put("street", addressLine1);
            put("city", user.getAddress().getCity());
            put("zip", user.getAddress().getZip());
            put("country_id", user.getAddress().getCountryId());
            put("phone", user.getPhone().replaceAll("\\s", ""));
        }};
    }

    @Override
    public User transformToObject(HashMap<String, Object> map) {
        User user = new User();
        Address address = new Address();
        if(map.get("id") instanceof Integer id)
            user.setId(id);
        if(map.get("name") instanceof String name)
            user.setFullName(name);
        if(map.get("email") instanceof String email)
            user.setEmail(email);
        if(map.get("phone") instanceof String phone)
            user.setPhone(phone.replaceAll("\\s", ""));
        if(map.get("street") instanceof String addressLine1) {
            addressLine1 = addressLine1.trim();
            int index = addressLine1.lastIndexOf(" ");
            if(index > 0) {
                address.setStreet(addressLine1.substring(0, index));
                address.setNumber(addressLine1.substring(index + 1));
            }
        }
        if(map.get("city") instanceof String city)
            address.setCity(city);
        if(map.get("zip") instanceof String zip)
            address.setZip(zip);
        if(map.get("country_id") instanceof Object[] country) {
            address.setCountry((String) country[1]);
            address.setCountryId((int) country[0]);
        }
        user.setAddress(address);

        return user;
    }

}