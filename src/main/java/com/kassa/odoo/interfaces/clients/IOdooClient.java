package com.kassa.odoo.interfaces.clients;

import com.kassa.models.Entity;
import com.kassa.models.exceptions.RecordNotFoundException;
import com.kassa.models.exceptions.UnsupportedMessageException;
import org.apache.xmlrpc.XmlRpcException;

import java.util.List;

public interface IOdooClient<T extends Entity> {
    T get(int id) throws RecordNotFoundException, XmlRpcException;

    List<T> getRange(List<Integer> ids) throws RecordNotFoundException, XmlRpcException;

    int searchIdByName(String name) throws RecordNotFoundException, XmlRpcException;

    int create(T t) throws UnsupportedMessageException;

    void update(T t) throws XmlRpcException, UnsupportedMessageException;

    void archive(int id) throws XmlRpcException;

    void delete(int id) throws XmlRpcException;
}
