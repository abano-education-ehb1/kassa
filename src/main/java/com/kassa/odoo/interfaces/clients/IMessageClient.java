package com.kassa.odoo.interfaces.clients;

import com.kassa.models.Message;
import org.apache.xmlrpc.XmlRpcException;

import java.util.List;

public interface IMessageClient {
    List<Message> getUnreadMessages() throws XmlRpcException;

    void deleteMessagesByIds(List<Integer> ids) throws XmlRpcException;

    void archiveMessagesByIds(List<Integer> ids) throws XmlRpcException;
}
