package com.kassa.odoo.clients;

import com.kassa.models.User;
import com.kassa.models.exceptions.UnsupportedMessageException;
import com.kassa.odoo.transformaters.UserTransformator;
import lombok.extern.log4j.Log4j2;
import org.apache.xmlrpc.XmlRpcException;

import java.util.List;

@Log4j2
public class UserClient extends OdooClient<User> {
    private final CountryClient countryClient;

    public UserClient() {
        super(new UserTransformator(), "res.partner",
                List.of("name", "email", "phone", "street", "zip", "city", "country_id"));

        countryClient = new CountryClient();
    }

    @Override
    public int create(User user) throws UnsupportedMessageException {
        try {
            countryClient.setCountryId(user.getAddress());
        } catch (XmlRpcException | UnsupportedMessageException e) {
            throw new UnsupportedMessageException("Unable to create user", e);
        }
        return super.create(user);
    }

    @Override
    public void update(User user) throws XmlRpcException, UnsupportedMessageException {
        try {
            countryClient.setCountryId(user.getAddress());
        } catch (UnsupportedMessageException e) {
            throw new UnsupportedMessageException("Unable to update user", e);
        }
        super.update(user);
    }
}
