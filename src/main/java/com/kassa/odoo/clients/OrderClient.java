package com.kassa.odoo.clients;

import com.kassa.models.Order;
import com.kassa.models.OrderLine;
import com.kassa.models.exceptions.RecordNotFoundException;
import com.kassa.odoo.transformaters.OrderLineTransformator;
import com.kassa.odoo.transformaters.OrderTransformator;
import org.apache.xmlrpc.XmlRpcException;

import java.util.List;
import java.util.stream.Collectors;

public class OrderClient extends OdooClient<Order> {
    private final OdooClient<OrderLine> orderLineClient;
    private final CompanyClient companyClient;

    public OrderClient() {
        super(new OrderTransformator(), "pos.order", List.of("date_order", "amount_total", "partner_id", "to_invoice", "lines", "state"));

        orderLineClient = new OdooClient<>(
                new OrderLineTransformator(),
                "pos.order.line",
                List.of("order_id", "product_id", "qty", "price_unit")
        );

        companyClient = new CompanyClient();
    }

    /**
     * This method overrides the default implementation of <code>getRange</code>, so that
     * <ul>
     *     <li>the according orderLines needs to be fetched to and added to the order-object</li>
     *     <li>it can be double checked whether an invoice is needed</li>
     * </ul>
     *
     * @param ids a list of ids where the records that needs to be fetched from Odoo
     * @return List of orders that matches the specified ids
     */
    @Override
    public List<Order> getRange(List<Integer> ids) throws XmlRpcException {
        try {
            List<Order> orders = super.getRange(ids);
            for (Order order: orders) {
                List<Integer> orderLineIds = order.getOrderLines().stream().map(OrderLine::getId).collect(Collectors.toList());
                order.setOrderLines(orderLineClient.getRange(orderLineIds));
                boolean isCompany = companyClient.isCompany(order.getUserId());
                boolean toInvoice = order.isToInvoice() || isCompany;
                order.setToInvoice(toInvoice);
                order.setPaid(!isCompany);
            }
            return orders;
        } catch (RecordNotFoundException e) {
            throw new RecordNotFoundException("Unable to find the matching orders", e);
        }
    }

    @Override
    public int create(Order order) {
        throw new UnsupportedOperationException("It is not possible to create a Order via the XML-RPC client");
    }

    @Override
    public void update(Order order) {
        throw new UnsupportedOperationException("It is not possible to update a Order via the XML-RPC client");
    }

    @Override
    public void archive(int id) {
        throw new UnsupportedOperationException("It is not possible to delete a Order via the XML-RPC client");
    }
}
