package com.kassa.odoo.clients;

import com.kassa.models.Company;
import com.kassa.models.exceptions.RecordNotFoundException;
import com.kassa.models.exceptions.UnsupportedMessageException;
import com.kassa.odoo.transformaters.CompanyTransformator;
import lombok.extern.log4j.Log4j2;
import org.apache.xmlrpc.XmlRpcException;

import java.util.HashMap;
import java.util.List;

@Log4j2
public class CompanyClient extends OdooClient<Company> {
    private final CountryClient countryClient;

    public CompanyClient() {
        super(new CompanyTransformator(), "res.partner", List.of("name", "email", "phone", "street", "zip", "city", "country_id", "vat"));

        countryClient = new CountryClient();
    }


    @Override
    public int create(Company company) throws UnsupportedMessageException {
        try {
            countryClient.setCountryId(company.getAddress());
        } catch (XmlRpcException | UnsupportedMessageException e) {
            String message = "Unable to create company";
            throw new UnsupportedMessageException(message, e);
        }
        return super.create(company);
    }

    @Override
    public void update(Company company) throws XmlRpcException, UnsupportedMessageException {
        try {
            countryClient.setCountryId(company.getAddress());
        } catch (RecordNotFoundException e) {
            String message = "Unable to create company";
            throw new UnsupportedMessageException(message, e);
        }
        super.update(company);
    }

    /**
     * Checks whether the given id represents a country or user
     *
     * @param id the id of a company or user
     * @return true whether it is a company, false otherwise
     * @throws XmlRpcException Thrown when something went wrong while trying to communicate with Odoo
     */
    public boolean isCompany(int id) throws XmlRpcException {
        XmlRpcClientInitializer initializer = XmlRpcClientInitializer.getInstance();
        List<Object> results = List.of((Object[]) getModels().execute("execute_kw",
                List.of(initializer.getDb(), initializer.getUid(), initializer.getKey(), "res.partner",
                        "read", List.of(id), new HashMap<String, List<String>>() {{
                            put("fields", List.of("is_company"));
                        }})));
        if (results.isEmpty())
            return false; // no customer found with the given id
        if (((HashMap<String, Object>) results.get(0)).get("is_company") instanceof Boolean isCompany) {
            return isCompany;
        }
        return false;
    }
}
