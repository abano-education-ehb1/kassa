package com.kassa.odoo.clients;

import com.kassa.models.Category;
import com.kassa.odoo.transformaters.CategoryTransformator;

import java.util.List;

public class CategoryClient extends OdooClient<Category> {
    public CategoryClient() {
        super(new CategoryTransformator(), "pos.category", List.of("name", "parent_id"));
    }
}
