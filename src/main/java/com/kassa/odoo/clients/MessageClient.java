package com.kassa.odoo.clients;

import com.kassa.models.Message;
import com.kassa.odoo.interfaces.clients.IMessageClient;
import com.kassa.odoo.transformaters.MessageTransformator;
import com.kassa.odoo.transformaters.Transformator;
import org.apache.xmlrpc.XmlRpcException;
import org.apache.xmlrpc.client.XmlRpcClient;

import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

public class MessageClient implements IMessageClient {
    private final OdooClient.XmlRpcClientInitializer initializer;
    private final XmlRpcClient models;
    private final Transformator<Message> transformator;

    public MessageClient() {
        initializer = OdooClient.XmlRpcClientInitializer.getInstance();
        models = initializer.getModelsClient();
        transformator = new MessageTransformator();
    }

    /**
     * @return all the messages that aren't read yet
     */
    @Override
    public List<Message> getUnreadMessages() throws XmlRpcException {
        try {
            List<Object> response = List.of((Object[]) models.execute("execute_kw", List.of(
                    initializer.getDb(), initializer.getUid(), initializer.getKey(), "x_message",
                    "search_read", List.of(List.of(List.of("x_read", "=", false))),
                    new HashMap<String, List<String>>() {{
                        put("fields", List.of("x_entity", "x_action", "x_rel_id"));
                    }}
            )));
            return response.stream()
                    .map(e -> (HashMap<String, Object>) e)
                    .map(transformator::transformToObject)
                    .collect(Collectors.toList());
        } catch (XmlRpcException e) {
            throw new XmlRpcException("Something went wrong while trying to fetch the unread messages from Odoo", e);
        }
    }

    /**
     * This will archive or soft delete all the messages with the specified ids
     *
     * @param ids the ids of all the messages that should be archived
     */
    @Override
    public void archiveMessagesByIds(List<Integer> ids) throws XmlRpcException {
        try {
            models.execute("execute_kw", List.of(
                    initializer.getDb(), initializer.getUid(), initializer.getKey(), "x_message", "write",
                    List.of(ids, new HashMap<String, Object>() {{
                                put("x_read", true);
                            }}
                    )
            ));
        } catch (XmlRpcException e) {
           throw new XmlRpcException("Something went wrong while trying to archive a document in Odoo", e);
        }
    }

    /**
     * This will hard delete all the messages with the specified ids
     *
     * @param ids the ids of all the messages that should be archived
     */
    @Override
    public void deleteMessagesByIds(List<Integer> ids) throws XmlRpcException {
        try {
            models.execute("execute_kw", List.of(
                    initializer.getDb(), initializer.getUid(), initializer.getKey(), "x_message", "unlink",
                    List.of(ids)
            ));
        } catch (XmlRpcException e) {
            throw new XmlRpcException("Something went wrong while trying to delete certain messages from Odoo", e);
        }
    }
}
