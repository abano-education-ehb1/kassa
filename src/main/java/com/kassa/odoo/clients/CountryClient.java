package com.kassa.odoo.clients;

import com.kassa.models.Address;
import com.kassa.models.exceptions.RecordNotFoundException;
import com.kassa.models.exceptions.UnsupportedMessageException;
import com.kassa.odoo.transformaters.Transformator;
import org.apache.xmlrpc.XmlRpcException;

import java.util.HashMap;
import java.util.List;

public class CountryClient extends OdooClient<Address.Country> {
    public CountryClient() {
        super(new CountryTransformator(), "res.country", List.of("name"));
    }

    /**
     * Sets the id of the country in the address, which is sometimes needed by Odoo
     *
     * @param address The address where from the country id needs to be set
     * @throws XmlRpcException             Thrown when an exception occurred during between Odoo and the call
     * @throws UnsupportedMessageException Thrown when the record was not found in Odoo
     */
    public void setCountryId(Address address) throws XmlRpcException, UnsupportedMessageException {
        try {
            if (address.getCountryId() == 0) {
                address.setCountryId(this.searchIdByName(address.getCountry().trim()));
            }
        } catch (RecordNotFoundException e) {
            throw new UnsupportedMessageException("Country \"" + address.getCountry() + "\" was not found in Odoo", e);
        }
    }

    /**
     * Inner static class, because of the little code and minor interest in this transformator
     */
    private static class CountryTransformator implements Transformator<Address.Country> {
        @Override
        public HashMap<String, Object> transformToMap(Address.Country country) {
            return new HashMap<>() {{
                put("id", country.getId());
                put("name", country.getName());
            }};
        }

        @Override
        public Address.Country transformToObject(HashMap<String, Object> map) {
            return new Address.Country((int) map.get("id"), (String) map.get("name"));
        }
    }
}
