package com.kassa.odoo.clients;

import com.kassa.models.Entity;
import com.kassa.models.dtos.Error;
import com.kassa.models.exceptions.RecordNotFoundException;
import com.kassa.models.exceptions.UnsupportedMessageException;
import com.kassa.odoo.interfaces.clients.IOdooClient;
import com.kassa.odoo.transformaters.Transformator;
import com.kassa.rabbitmq.Sender;
import com.kassa.util.CredentialsLoader;
import lombok.extern.log4j.Log4j2;
import org.apache.xmlrpc.XmlRpcException;
import org.apache.xmlrpc.client.XmlRpcClient;
import org.apache.xmlrpc.client.XmlRpcClientConfigImpl;

import java.io.IOException;
import java.net.URL;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;

@Log4j2
public class OdooClient<T extends Entity> implements IOdooClient<T> {
    private final Transformator<T> transformator;
    private final String model;
    private final List<String> fields;
    private final XmlRpcClientInitializer initializer;
    private final XmlRpcClient models;

    public OdooClient(Transformator<T> transformator, String model, List<String> fields) {
        this.transformator = transformator;
        this.model = model;
        this.fields = fields;
        initializer = XmlRpcClientInitializer.getInstance();
        models = initializer.getModelsClient();
    }

    /**
     * This method will search for a single record or object out of the db of Odoo
     *
     * @param id the id of the record that must be returned
     * @return The record that matches the specified id
     * @throws RecordNotFoundException Thrown when the specified id does not match any record in Odoo
     */
    @Override
    public T get(int id) throws RecordNotFoundException, XmlRpcException {
        try {
            List<T> tList = getRange(List.of(id));
            return tList.get(0);
        } catch (RecordNotFoundException e) {
            throw new RecordNotFoundException("No records found in \"" + model + "\" that corresponds with the id " + id);
        }
    }

    /**
     * Fetches a list of all the records that matches the specified ids
     *
     * @param ids a list of ids where the records that needs to be fetched from Odoo
     * @return A list of the records that matches te specified ids
     * @throws RecordNotFoundException Thrown when no single id matches any record in Odoo
     */
    @Override
    public List<T> getRange(List<Integer> ids) throws RecordNotFoundException, XmlRpcException {
        List<Object> results = List.of((Object[]) models.execute("execute_kw",
                List.of(initializer.getDb(), initializer.getUid(), initializer.getKey(), model,
                        "read", List.of(ids), new HashMap<String, List<String>>() {{
                            put("fields", fields);
                        }}
                ))
        );
        List<T> tList = results.stream()
                .map(e -> (HashMap<String, Object>) e)
                .map(transformator::transformToObject).toList();
        if(tList.isEmpty())
            throw new RecordNotFoundException("No records found in \"" + model + "\" that corresponds with following ids " + ids);
        else return tList;
    }

    /**
     * This method will search the id of an entity via its name
     *
     * @param name Name of the entity from which the id needs to be searched
     * @return the id of the searched entity or -1 if the searching failed
     * @throws RecordNotFoundException Thrown when no records is found that matches the given name
     */
    @Override
    public int searchIdByName(String name) throws RecordNotFoundException, XmlRpcException {
        try {
            return (int) ((Object[]) models.execute("execute_kw",
                    List.of(initializer.getDb(), initializer.getUid(), initializer.getKey(), model,
                            "search", List.of(List.of(List.of("name", "=", name)))
                    )))[0];
        } catch (ArrayIndexOutOfBoundsException e) {
            String message = "No record found in \"" + model + "\" with the name \"" + name + "\"";
            throw new RecordNotFoundException(message);
        } catch (XmlRpcException e) {
            throw new XmlRpcException("Something went wrong while trying to search a document by name from Odoo", e);
        }
    }

    /**
     * @param t entity to be created at the Odoo API
     * @return id of the created entity or -1 if creation failed
     */
    @Override
    public int create(T t) throws UnsupportedMessageException {
        try {
            return (int) models.execute("execute_kw", List.of(
                    initializer.getDb(), initializer.getUid(), initializer.getKey(), model, "create",
                    List.of(transformator.transformToMap(t)))
            );
        } catch (XmlRpcException | ClassCastException e) {
            String message = "Something went wrong while trying to create an instance of "
                    + t.getClass().getSimpleName().toLowerCase() + " in Odoo";
            throw new UnsupportedMessageException(message, e);
        }

    }

    @Override
    public void update(T t) throws XmlRpcException, UnsupportedMessageException {
        try {
            models.execute("execute_kw", List.of(
                    initializer.getDb(), initializer.getUid(), initializer.getKey(), model, "write",
                    List.of(List.of(t.getId()), transformator.transformToMap(t))
            ));
        } catch (XmlRpcException e) {
            String message = "Something went wrong while trying to update a \""
                    + t.getClass().getSimpleName().toLowerCase() + "\" document in Odoo";
            throw new XmlRpcException(message, e);
        }
    }

    @Override
    public void archive(int id) throws XmlRpcException {
        try {
            models.execute("execute_kw", List.of(
                    initializer.getDb(), initializer.getUid(), initializer.getKey(), model, "write",
                    List.of(List.of(id), new HashMap<String, Object>() {{
                        put("active", false);
                    }})
            ));
        } catch (XmlRpcException e) {
            String message = "Something went wrong while trying to archive a document in Odoo";
            throw new XmlRpcException(message, e);
        }
    }

    @Override
    public void delete(int id) throws XmlRpcException {
        try {
            models.execute("execute_kw", List.of(
                    initializer.getDb(), initializer.getUid(), initializer.getKey(), model, "unlink",
                    List.of(List.of(id))
            ));
        } catch (XmlRpcException e) {
            String message = "Something went wrong while trying to delete a document in Odoo";
            throw new XmlRpcException(message, e);
        }
    }

    protected XmlRpcClient getModels() {
        return models;
    }


    protected final static class XmlRpcClientInitializer {
        private final Properties credentials;
        private final int uid;
        private final XmlRpcClient models;

        private static XmlRpcClientInitializer INSTANCE;

        private XmlRpcClientInitializer() throws IOException, XmlRpcException {
            credentials = CredentialsLoader.getCredentials();

            final XmlRpcClient client = new XmlRpcClient() {{
                setConfig(new XmlRpcClientConfigImpl() {{
                    setServerURL(new URL(String.format("%s/xmlrpc/common", credentials.getProperty("odoo.host"))));
                }});
            }};
            uid = (int) client.execute("authenticate",
                    List.of(credentials.getProperty("odoo.db"), credentials.getProperty("odoo.username"), credentials.getProperty("odoo.key"), Collections.emptyMap()));
            models = new XmlRpcClient() {{
                setConfig(new XmlRpcClientConfigImpl() {{
                    setServerURL(new URL(String.format("%s/xmlrpc/2/object", credentials.getProperty("odoo.host"))));
                    setEnabledForExtensions(true);
                }});
            }};
        }

        public static XmlRpcClientInitializer getInstance() {
            if (INSTANCE == null) {
                try {
                    INSTANCE = new XmlRpcClientInitializer();
                } catch (IOException | XmlRpcException e) {
                    log.error(e);
                    new Sender().sendError(Error.Level.ERROR, e.getMessage());
                }
            }
            return INSTANCE;
        }

        public String getDb() {
            return credentials.getProperty("odoo.db");
        }

        public String getKey() {
            return credentials.getProperty("odoo.key");
        }

        public int getUid() {
            return uid;
        }

        public XmlRpcClient getModelsClient() {
            return models;
        }
    }

}
