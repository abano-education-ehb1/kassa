package com.kassa.odoo.clients;

import com.kassa.models.Product;
import com.kassa.odoo.transformaters.ProductTransformator;

import java.util.List;

public class ProductClient extends OdooClient<Product> {
    public ProductClient() {
        super(new ProductTransformator(), "product.template", List.of("name", "list_price", "pos_categ_id"));
    }
}
