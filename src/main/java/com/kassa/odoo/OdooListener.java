package com.kassa.odoo;

import com.kassa.models.Entity;
import com.kassa.models.Message;
import com.kassa.models.dtos.Error;
import com.kassa.models.dtos.*;
import com.kassa.models.exceptions.RecordNotFoundException;
import com.kassa.models.exceptions.UnsupportedMessageException;
import com.kassa.odoo.clients.CompanyClient;
import com.kassa.odoo.clients.OrderClient;
import com.kassa.odoo.clients.UserClient;
import com.kassa.odoo.interfaces.clients.IMessageClient;
import com.kassa.odoo.interfaces.clients.IOdooClient;
import com.kassa.rabbitmq.interfaces.ISender;
import lombok.extern.log4j.Log4j2;
import org.apache.xmlrpc.XmlRpcException;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@Log4j2
public class OdooListener {
    private final Runnable listenRunnable;
    private final IMessageClient messageClient;
    private final ISender sender;

    /**
     * @param messageClient the messageClient that needs to be injected
     * @param sender        the sender that needs to be injected
     */
    public OdooListener(IMessageClient messageClient, ISender sender) {
        this.messageClient = messageClient;
        this.sender = sender;

        listenRunnable = () -> {
            try {
                List<Message> unreadMessages = messageClient.getUnreadMessages();
                if (!unreadMessages.isEmpty()) {
                    handleMessages(unreadMessages);
                }
            } catch (XmlRpcException e) {
                String message = "Something went wrong while trying to fetch the unread messages from Odoo";
                sender.sendError(message, e);
            }
        };
    }


    /**
     * This method transforms a list of messages to a DataEnveloppe and enriches these with the related documents from Odoo
     *
     * @param messages The list of messages to transform and enrich
     */
    protected void handleMessages(List<Message> messages) {
        List<Integer> idsToDelete = new ArrayList<>();
        for (Message message: messages) {
            try {
                switch (message.getEntity()) {
                    case "user" -> {
                        UserDto dto = new UserDto();
                        UserClient client = new UserClient();
                        handleSingleMessage("user", client, message, dto);
                    }
                    case "company" -> {
                        CompanyDto dto = new CompanyDto();
                        CompanyClient client = new CompanyClient();
                        handleSingleMessage("company", client, message, dto);
                    }
                    case "order" -> {
                        OrderDto dto = new OrderDto();
                        OrderClient client = new OrderClient();
                        handleSingleMessage("order", client, message, dto);
                    }
                    default -> {
                    }
                }
                idsToDelete.add(message.getId());
            } catch (UnsupportedMessageException e) {
                log.error(e.getMessage());
                sender.sendError(Error.Level.ERROR, e);
            }
        }
        try {
            messageClient.archiveMessagesByIds(idsToDelete);
        } catch (XmlRpcException e) {
            String message = "Something went wrong while trying to archive a document in Odoo " + e.getMessage();
            sender.sendError(message, e);
        }
    }

    /**
     * Kind of a wrapper function that deals with the exceptions and error logging
     */
    protected <T extends Entity> void handleSingleMessage(String entity, IOdooClient<T> client, Message message, EntityDto<T> dto) throws UnsupportedMessageException {
        enrichDto(client, message, dto);
        sender.sendData(dto, entity);
    }

    private <T extends Entity> void enrichDto(IOdooClient<T> client, Message message, EntityDto<T> dto) throws UnsupportedMessageException {
        try {
            T t = client.get(message.getRelatedId());
            dto.setProperties(t);
        } catch (XmlRpcException | RecordNotFoundException e) {
            throw new UnsupportedMessageException("Unable to find the according record in Odoo", e);
        }
        dto.setSource("kassa");
        dto.setAction(message.getAction());
        dto.setSourceId(message.getRelatedId());
    }

    public Runnable getListenRunnable() {
        return listenRunnable;
    }

    public void listen() {
        ScheduledExecutorService executor = Executors.newScheduledThreadPool(1);
        try {
            executor.scheduleAtFixedRate(listenRunnable, 0, 10, TimeUnit.SECONDS);
        } catch (RuntimeException e) {
            String message = "Unable to listen to Odoo";
            sender.sendError(message, e);
        }
        log.info("System is now listening to Odoo");
    }
}
