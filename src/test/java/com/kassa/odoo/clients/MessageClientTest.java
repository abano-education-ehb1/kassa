package com.kassa.odoo.clients;

import com.kassa.models.Message;
import com.kassa.odoo.interfaces.clients.IMessageClient;
import org.apache.xmlrpc.XmlRpcException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.easymock.EasyMock.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

class MessageClientTest {
    private IMessageClient mockedMessageClient;

    @BeforeEach
    public void setUp() {
        mockedMessageClient = mock(IMessageClient.class);
    }

    @Test
    public void archiveMessagesByIds() {
        List<Message> messages = new ArrayList<>();
        messages.add(new Message() {{
            setEntity("user");
            setAction("create");
            setId(111);
            setRelatedId(432);
        }});
        messages.add(new Message() {{
            setEntity("user");
            setAction("update");
            setId(112);
            setRelatedId(432);
        }});
        messages.add(new Message() {{
            setEntity("company");
            setAction("delete");
            setId(113);
            setRelatedId(429);
        }});

       try {
           expect(mockedMessageClient.getUnreadMessages())
                   .andReturn(messages)
                   .once()
                   .andReturn(new ArrayList<>());
       } catch (XmlRpcException e) {
           fail("Unexpected exception was thrown", e);
       }

       try {
           mockedMessageClient.archiveMessagesByIds(messages.stream().map(Message::getId).toList());
           replay(mockedMessageClient);
           assertEquals(messages, mockedMessageClient.getUnreadMessages());
           mockedMessageClient.archiveMessagesByIds(messages.stream().map(Message::getId).toList());
           assertEquals(new ArrayList<>(), mockedMessageClient.getUnreadMessages());
       } catch (XmlRpcException e) {
           fail("Unexpected exception was thrown", e);
       }

        verify(mockedMessageClient);
    }
}