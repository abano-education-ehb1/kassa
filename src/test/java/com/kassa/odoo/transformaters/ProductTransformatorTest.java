package com.kassa.odoo.transformaters;

import com.kassa.models.Category;
import com.kassa.models.Product;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.*;

class ProductTransformatorTest {
    private ProductTransformator transformator;
    private Product product;
    private Category category;

    @BeforeEach
    void setUp() {
        transformator = new ProductTransformator();
        category = new Category(2, "Drinks", 0);
        product = new Product(2, "Ice tea", 2.0, 21.0, category);
    }

    @Test
    void transformToMap() {
        HashMap<String, Object> transform = transformator.transformToMap(product);
        assertEquals(product.getName(), transform.get("name"));
        assertEquals(product.getSalesPrice(), transform.get("list_price"));
        assertEquals(product.getCategory().getId(), transform.get("pos_categ_id"));
    }

    @Test
    void transformToObject() {
        HashMap<String, Object> map = new HashMap<>() {{
            put("name", product.getName());
            put("id", product.getId());
            put("list_price", product.getSalesPrice());
            put("pos_categ_id", product.getCategory().getId());
        }};

    }
}