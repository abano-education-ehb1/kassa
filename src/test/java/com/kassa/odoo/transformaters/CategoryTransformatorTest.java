package com.kassa.odoo.transformaters;

import com.kassa.models.Category;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.*;

class CategoryTransformatorTest {

    private CategoryTransformator categoryTransformator;
    private Category category;


    @BeforeEach
    void setUp() {
        categoryTransformator = new CategoryTransformator();
        category = new Category(2, "Drinks", 0);
    }

    @Test
    void transformToMap() {
        HashMap<String, Object> transform = categoryTransformator.transformToMap(category);
        assertEquals(category.getName(), transform.get("name"));
        assertEquals(category.getParentId(), transform.get("parent_id"));
    }

    @Test
    void transformToObject() {
        HashMap<String, Object> map = new HashMap<>() {{
            put("name", category.getName());
            put("id", category.getId());
        }};
        Category category = categoryTransformator.transformToObject(map);
        assertEquals(map.get("name"), category.getName());
        // assertEquals(map.get("parent_id"), category.getParentId());
        assertEquals(map.get("id"), category.getId());
    }
}