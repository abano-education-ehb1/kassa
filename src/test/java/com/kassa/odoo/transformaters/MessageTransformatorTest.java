package com.kassa.odoo.transformaters;

import com.kassa.models.Category;
import com.kassa.models.Message;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.HashMap;

class MessageTransformatorTest {
    private MessageTransformator transformator;
    private Message message;

    @BeforeEach
    void setUp() {
        transformator = new MessageTransformator();
        message = new Message(2, "delete", "product", 3, new Category(2, "Product", 3));
    }


    @Test
    void transformToObject() {
        HashMap<String, Object> map = new HashMap<>();
        map.put("id", message.getId());
        map.put("x_rel_id", message.getRelatedId());
        map.put("x_entity", message.getEntity());
        map.put("x_action", message.getAction());
    }
}