package com.kassa.odoo.transformaters;

import com.kassa.models.Address;
import com.kassa.models.Company;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.*;

class CompanyTransformatorTest {
    private CompanyTransformator companyTransformator;
    private Address.Country country;
    private Address address;
    private Company company;

    @BeforeEach
    void setUp() {
        companyTransformator = new CompanyTransformator();
        country = new Address.Country(2, "Belgium");
        address = new Address("Nijverheidskaai", "25", "Nijverheidskaai 25", "Anderlecht", "1070", country);
        company = new Company(2, "Deloitte", address, "4", "0456981236", "ehb@company.be");
    }

    @Test
    void transformToMap() {
        HashMap<String, Object> transform = companyTransformator.transformToMap(company);
        assertEquals(company.getName(), transform.get("name"));
        assertEquals(company.getEmail(), transform.get("email"));
        assertEquals(company.getPhone(), transform.get("phone"));
        assertEquals(address.getZip(), transform.get("zip"));
    }

    @Test
    void transformToObject() {
        HashMap<String, Object> map = new HashMap<>();
        map.put("email", company.getEmail());
        map.put("id", company.getId());
        map.put("phone", company.getPhone());
        map.put("vat", company.getTaxId());
        map.put("street", company.getAddress().getStreet() + " " + company.getAddress().getNumber());
        map.put("city", company.getAddress().getCity());
        map.put("zip", company.getAddress().getZip());
        map.put("country_id", company.getAddress().getCountryId());

        Company company = companyTransformator.transformToObject(map);
        assertEquals(map.get("email"), company.getEmail());
        assertEquals(map.get("id"), company.getId());
        assertEquals(map.get("phone"), company.getPhone());
        assertEquals(map.get("vat"), company.getTaxId());
        assertEquals(map.get("street"), company.getAddress().getStreet() + " " + company.getAddress().getNumber());
        assertEquals(map.get("city"), company.getAddress().getCity());
        assertEquals(map.get("zip"), company.getAddress().getZip());
    }
}