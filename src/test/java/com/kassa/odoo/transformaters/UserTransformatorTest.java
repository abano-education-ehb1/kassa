package com.kassa.odoo.transformaters;

import com.kassa.models.Address;
import com.kassa.models.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.*;

class UserTransformatorTest {
    private UserTransformator transformator;
    private Address.Country country;
    private Address address;
    private User user;


    @BeforeEach
    void setUp() {
        transformator = new UserTransformator();
        country = new Address.Country(2, "Belgium");
        address = new Address("Nijverheidskaai", "25", "Nijverheidskaai 25", "Anderlecht", "1070", country);
        user = new User(1, "Tevhide", "Camur", "tevhide@student.be", address, "0465895635");
    }

    @Test
    void transformToMap() {
        HashMap<String, Object> transform = transformator.transformToMap(user);
        assertEquals(user.getPhone(), transform.get("phone"));
        assertEquals(user.getEmail(), transform.get("email"));
        assertEquals(user.getAddress().getStreet() + " " + user.getAddress().getNumber(), transform.get("street"));
        assertEquals(user.getFullName(), transform.get("name"));
        assertEquals(user.getAddress().getCity(), transform.get("city"));
        assertEquals(user.getAddress().getZip(), transform.get("zip"));
        assertEquals(user.getAddress().getCountryId(), transform.get("country_id"));

    }

    @Test
    void transformToObject() {
        HashMap<String, Object> map = new HashMap<>() {{
            put("id", user.getId());
            put("name", user.getFullName());
            put("email", user.getEmail());
            put("phone", user.getPhone());
            put("street", user.getAddress().getStreet() + " " + user.getAddress().getNumber());
            put("city", user.getAddress().getCity());
            put("zip", user.getAddress().getZip());
            put("country_id", user.getAddress().getCountry());
        }};
    }
}