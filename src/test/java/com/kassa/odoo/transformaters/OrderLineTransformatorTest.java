package com.kassa.odoo.transformaters;

import com.kassa.models.OrderLine;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

class OrderLineTransformatorTest {
    private OrderLineTransformator transformator;
    private OrderLine orderLine;


    @BeforeEach
    void setUp() {
        List<OrderLine> orderLines = new ArrayList<>();
        transformator = new OrderLineTransformator();
        orderLine = new OrderLine(2, 5, 5.0, "Drinks");
        orderLines.add(orderLine);
    }


    @Test
    void transformToObject() {
        HashMap<String, Object> map = new HashMap<>() {{
            put("id", orderLine.getId());
            put("product_id", orderLine.getId());
            put("qty", orderLine.getQuantity());
            put("price_unit", orderLine.getUnityPrice());
        }};
    }
}