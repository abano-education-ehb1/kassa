package com.kassa.odoo.transformaters;

import com.kassa.models.Identifiers;
import com.kassa.models.Order;
import com.kassa.models.OrderLine;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class OrderTransformatorTest {
    private OrderTransformator transformator;
    private OrderLine orderLine;
    private Order order;

    @BeforeEach
    void setUp() {
        transformator = new OrderTransformator();
        List<OrderLine> orderLines = new ArrayList<>();
        order = new Order(2, LocalDateTime.now(), false, true, 50.0, new Identifiers(23), orderLines);
    }

    @Test
    void transformToMap() {
        List<OrderLine> orderLines = new ArrayList<>();
        HashMap<String, Object> map = transformator.transformToMap(order);
        assertEquals(order.getId(), map.get("id"));
        //assertEquals(order.getDate(),map.get("date_order"));
        assertEquals(order.getTotal(), map.get("amount_total"));
        //assertEquals(order.getUserIds().getSourceId(),map.get("partner_id"));
        assertEquals(order.isToInvoice(), map.get("to_invoice"));
        assertEquals(order.getOrderLines(), map.get("lines"));
    }

    @Test
    void transformToObject() {
        HashMap<String, Object> map = new HashMap<>() {{
            put("id", order.getId());
            put("date_order", order.getDate());
            put("amount_total", order.getTotal());
            put("parent_id", order.getUserId());
            put("to_invoice", order.isToInvoice());
            put("lines", order.getOrderLines());
        }};
    }
}