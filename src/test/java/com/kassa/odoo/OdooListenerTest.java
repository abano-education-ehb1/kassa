package com.kassa.odoo;

import com.kassa.models.Company;
import com.kassa.models.Message;
import com.kassa.models.Order;
import com.kassa.models.User;
import com.kassa.models.dtos.CompanyDto;
import com.kassa.models.dtos.OrderDto;
import com.kassa.models.dtos.UserDto;
import com.kassa.models.exceptions.RecordNotFoundException;
import com.kassa.models.exceptions.UnsupportedMessageException;
import com.kassa.odoo.interfaces.clients.IMessageClient;
import com.kassa.odoo.interfaces.clients.IOdooClient;
import com.kassa.rabbitmq.interfaces.ISender;
import org.apache.xmlrpc.XmlRpcException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.util.Collections;
import java.util.List;

import static org.easymock.EasyMock.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class OdooListenerTest {
    private ISender mockedSender;
    private IMessageClient mockedMessageClient;
    private OdooListener odooListener;
    private Message userMessage;
    private Message companyMessage;
    private Message orderMessage;
    private IOdooClient<User> mockedUserClient;
    private IOdooClient<Company> mockedCompanyClient;
    private IOdooClient<Order> mockedOrderClient;
    private UserDto userDto;
    private CompanyDto companyDto;
    private OrderDto orderDto;
    private User user;
    private Company company;
    private Order order;


    @BeforeEach
    void setUp() {
        mockedSender = createMock(ISender.class);
        mockedMessageClient = createMock(IMessageClient.class);
        odooListener = new OdooListener(mockedMessageClient, mockedSender);

        mockedUserClient = createMock(IOdooClient.class);
        mockedCompanyClient = createMock(IOdooClient.class);
        mockedOrderClient = createMock(IOdooClient.class);

        userDto = createUserDto();
        companyDto = createCompanyDto();
        orderDto = createOrderDto();

        user = createUser();
        company = createCompany();
        order = createOrder();

        userMessage = new Message() {{
            setEntity("user");
            setAction("create");
            setId(111);
            setRelatedId(432);
        }};

        companyMessage = new Message() {{
            setEntity("company");
            setAction("create");
            setId(222);
            setRelatedId(243);
        }};

        orderMessage = new Message() {{
            setEntity("order");
            setAction("create");
            setId(333);
            setRelatedId(324);
        }};
    }

    @Test
    void userHandleSingleMessageSuccessful() throws XmlRpcException {
        expect(mockedUserClient.get(userMessage.getRelatedId())).andReturn(createUser());
        expect(mockedSender.sendData(userDto, "user")).andReturn(true);

        mockedMessageClient.archiveMessagesByIds(List.of(userMessage.getId()));
        expectLastCall().once();

        replay(mockedUserClient, mockedSender);

        odooListener = new OdooListener(mockedMessageClient, mockedSender);
        try {
            odooListener.handleSingleMessage("user", mockedUserClient, userMessage, userDto);
        } catch (UnsupportedMessageException e) {
            fail("Record should have been found in Odoo, but following exception was thrown: ", e);
        }

        verify(mockedUserClient);
        verify(mockedSender);

    }

    @Test
    void companyHandleSingleMessageSuccessful() throws XmlRpcException {
        expect(mockedCompanyClient.get(companyMessage.getRelatedId())).andReturn(createCompany());
        expect(mockedSender.sendData(companyDto, "company")).andReturn(true);

        mockedMessageClient.archiveMessagesByIds(List.of(companyMessage.getId()));
        expectLastCall().once();

        replay(mockedCompanyClient, mockedSender);

        odooListener = new OdooListener(mockedMessageClient, mockedSender);
        try {
            odooListener.handleSingleMessage("company", mockedCompanyClient, companyMessage, companyDto);
        } catch (UnsupportedMessageException e) {
            fail("Record should have been found in Odoo, but following exception was thrown: ", e);
        }

        verify(mockedCompanyClient);
        verify(mockedSender);

    }

    @Test
    void orderHandleSingleMessageSuccessful() throws XmlRpcException {
        expect(mockedOrderClient.get(orderMessage.getRelatedId())).andReturn(createOrder());
        expect(mockedSender.sendData(orderDto, "order")).andReturn(true);

        mockedMessageClient.archiveMessagesByIds(List.of(orderMessage.getId()));
        expectLastCall().once();

        replay(mockedOrderClient, mockedSender);

        odooListener = new OdooListener(mockedMessageClient, mockedSender);
        try {
            odooListener.handleSingleMessage("order", mockedOrderClient, orderMessage, orderDto);
        } catch (UnsupportedMessageException e) {
            fail("Record should have been found in Odoo, but following exception was thrown: ", e);
        }

        verify(mockedOrderClient);
        verify(mockedSender);

    }

    @Test
    void userHandleSingleMessageWithNonExistingRelatedDocument() {
        try {
            int relatedId = userMessage.getRelatedId();
            expect(mockedUserClient.get(relatedId))
                    .andThrow(new RecordNotFoundException("No records found in \"res.partner\" that corresponds with the id " + relatedId));

            mockedMessageClient.archiveMessagesByIds(List.of(userMessage.getId()));
            expectLastCall().once();
        } catch (XmlRpcException e) {
            fail("Unexpected exception was thrown: " + e.getMessage());
        }

        replay(mockedUserClient, mockedMessageClient);

        odooListener = new OdooListener(mockedMessageClient, mockedSender);
        try {
            odooListener.handleSingleMessage("user", mockedUserClient, userMessage, userDto);
            fail("An exception should have been thrown");
        } catch (UnsupportedMessageException e) {
            assertEquals("Unable to find the according record in Odoo", e.getMessage());
        }

        verify(mockedUserClient);
    }

    @Test
    void companyHandleSingleMessageWithNonExistingRelatedDocument() {
        try {
            int relatedId = companyMessage.getRelatedId();
            expect(mockedCompanyClient.get(relatedId))
                    .andThrow(new RecordNotFoundException("No records found in \"res.partner\" that corresponds with the id " + relatedId));

            mockedMessageClient.archiveMessagesByIds(List.of(companyMessage.getId()));
            expectLastCall().once();
        } catch (XmlRpcException e) {
            fail("Unexpected exception was thrown: " + e.getMessage());
        }

        replay(mockedCompanyClient, mockedMessageClient);

        odooListener = new OdooListener(mockedMessageClient, mockedSender);
        try {
            odooListener.handleSingleMessage("company", mockedCompanyClient, companyMessage, companyDto);
            fail("An exception should have been thrown");
        } catch (UnsupportedMessageException e) {
            assertEquals("Unable to find the according record in Odoo", e.getMessage());
        }

        verify(mockedCompanyClient);
    }

    @Test
    void orderHandleSingleMessageWithNonExistingRelatedDocument() {
        try {
            int relatedId = orderMessage.getRelatedId();
            expect(mockedOrderClient.get(relatedId))
                    .andThrow(new RecordNotFoundException("No records found in \"res.partner\" that corresponds with the id " + relatedId));

            mockedMessageClient.archiveMessagesByIds(List.of(orderMessage.getId()));
            expectLastCall().once();
        } catch (XmlRpcException e) {
            fail("Unexpected exception was thrown: " + e.getMessage());
        }

        replay(mockedOrderClient, mockedMessageClient);

        odooListener = new OdooListener(mockedMessageClient, mockedSender);
        try {
            odooListener.handleSingleMessage("order", mockedOrderClient, orderMessage, orderDto);
            fail("An exception should have been thrown");
        } catch (UnsupportedMessageException e) {
            assertEquals("Unable to find the according record in Odoo", e.getMessage());
        }

        verify(mockedOrderClient);
    }

    @Test
    void testNoMessagesToHandle() {
        try {
            expect(mockedMessageClient.getUnreadMessages()).andReturn(Collections.emptyList());
        } catch (XmlRpcException e) {
            fail("Unexpected exception was thrown", e);
        }

        replay(mockedMessageClient, mockedUserClient);

        odooListener = new OdooListener(mockedMessageClient, mockedSender);
        odooListener.getListenRunnable().run();

        verify(mockedMessageClient);
        verifyUnexpectedCalls(mockedUserClient);
    }


    private User createUser() {
        User user = new User();
        user.setId(432);
        user.setFullName("Jonas Bulcke");

        return user;
    }

    private Company createCompany() {
        Company company = new Company();
        company.setId(243);
        company.setName("EHB");

        return company;
    }

    private Order createOrder() {
        Order order = new Order();
        order.setId(324);
        order.setTotal(9.00);

        return order;
    }

    private UserDto createUserDto() {
        UserDto userDto = new UserDto();
        userDto.setProperties(createUser());
        userDto.setAction("create");
        userDto.setSource("kassa");
        userDto.setSourceId(432);

        return userDto;
    }

    private CompanyDto createCompanyDto() {
        CompanyDto companyDto = new CompanyDto();
        companyDto.setProperties(createCompany());
        companyDto.setAction("create");
        companyDto.setSource("kassa");
        companyDto.setSourceId(243);

        return companyDto;
    }

    private OrderDto createOrderDto() {
        OrderDto orderDto = new OrderDto();
        orderDto.setProperties(createOrder());
        orderDto.setAction("create");
        orderDto.setSource("kassa");
        orderDto.setSourceId(324);

        return orderDto;
    }
}