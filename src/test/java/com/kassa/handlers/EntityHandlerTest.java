package com.kassa.handlers;

import com.kassa.models.Address;
import com.kassa.models.User;
import com.kassa.models.dtos.Error;
import com.kassa.models.dtos.Response;
import com.kassa.models.dtos.UserDto;
import com.kassa.models.exceptions.UnsupportedMessageException;
import com.kassa.odoo.interfaces.clients.IOdooClient;
import com.kassa.rabbitmq.interfaces.ISender;
import org.apache.xmlrpc.XmlRpcException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.easymock.EasyMock.*;

class EntityHandlerTest {
    private EntityHandler<User> userHandler;
    private IOdooClient<User> mockedOdooClient;
    private ISender mockedSender;
    private Address.Country country;
    private Address address;

    private UserDto userDto;
    private int sourceId;

    @BeforeEach
    void setUp() {
        mockedOdooClient = createMock(IOdooClient.class);
        mockedSender = createMock(ISender.class);
        userHandler = new EntityHandler<User>(mockedOdooClient, mockedSender);
        country = new Address.Country("Belgium");
        address = new Address("Nijverheidskaai", "25", "Nijverheidskaai 25", "Anderlecht", "1070", country);

        userDto = new UserDto();
        sourceId = 2;
        userDto.setProperties(new User(sourceId, "Tevhide", "Camur", "tevhide@ehb.be", address, "0486963526"));
        userDto.setAction("create");
        userDto.setSourceId(sourceId);
    }

    @Test
    void createUser() throws UnsupportedMessageException {
        expect(mockedOdooClient.create(userDto.getProperties())).andReturn(sourceId);
        mockedSender.sendResponse(new Response(userDto));
        replay(mockedOdooClient, mockedSender);

        userHandler.handleDataEnveloppe(userDto);

        verify(mockedOdooClient);
        verify(mockedSender);
    }

    @Test
    void createUserExpectException() throws UnsupportedMessageException {
        String errorMessage = "Something went wrong while trying to create an instance of user in Odoo";
        expect(mockedOdooClient.create(userDto.getProperties())).andThrow(new UnsupportedMessageException(errorMessage, new XmlRpcException("Testing")));
        mockedSender.sendError(Error.Level.ERROR, errorMessage + ": Testing");
        replay(mockedOdooClient, mockedSender);

        userHandler.handleDataEnveloppe(userDto);

        verify(mockedOdooClient);
        verify(mockedSender);
    }
}