package com.kassa.util;

import com.kassa.models.*;
import com.kassa.models.dtos.CompanyDto;
import com.kassa.models.dtos.EntityDto;
import com.kassa.models.dtos.OrderDto;
import com.kassa.models.dtos.UserDto;
import com.kassa.models.exceptions.UnsupportedMessageException;
import com.kassa.models.exceptions.XmlParsingException;
import com.kassa.models.exceptions.XmlValidationException;
import jakarta.xml.bind.JAXBException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.*;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.params.provider.Arguments.arguments;

class XmlParserAndValidatorTest {
    private static byte[] userXmlBytes;
    private static UserDto userControlDto;

    private static byte[] companyXmlBytes;
    private static CompanyDto companyControlDto;

    private static byte[] orderXmlBytes;
    private static OrderDto orderControlDto;

    private final XmlValidator validator = new XmlValidator();

    /*
     *   - Loads the user xml from the resources folder
     *   - Creates UserDto object with the same values as in the xml for comparison
     */
    @BeforeAll
    static void init() throws IOException {
        initUserValues();
        initCompanyValues();
        initOrderValues();
    }

    //https://mkyong.com/junit5/junit-5-parameterized-tests/
    static Stream<Arguments> entityStringAndBytesProvider() {
        return Stream.of(
                arguments("user", userXmlBytes),
                arguments("company", companyXmlBytes),
                arguments("order", orderXmlBytes)
        );
    }

    @ParameterizedTest
    @MethodSource("entityStringAndBytesProvider")
    public void testGetRootElementTag(String entity, byte[] data) {
        try {
            assertEquals(entity, XmlParser.getRootElementTagName(data));
        } catch (XmlParsingException | UnsupportedMessageException e) {
            fail("Unexpected exception was thrown");
        }
    }


    static Stream<Arguments> dtoAndBytesProvider() {
        return Stream.of(
                arguments(userControlDto, userXmlBytes),
                arguments(companyControlDto, companyXmlBytes),
                arguments(orderControlDto, orderXmlBytes)
        );
    }

    @ParameterizedTest
    @MethodSource("dtoAndBytesProvider")
    public void testParseXmlToDto(EntityDto<Entity> dto, byte[] data) throws JAXBException {
        assertEquals(dto, XmlParser.xmlToData(data, dto.getClass()));
    }


    @ParameterizedTest
    @MethodSource("dtoAndBytesProvider")
    public <T extends Entity> void testParseDtoToXmlAndValidate(EntityDto<T> dto, byte[] data) {
        String entity = dto.getProperties().getClass().getSimpleName().toLowerCase(Locale.ROOT);
        try {
            String message = XmlParser.dataToXml(dto).replaceAll("\\r\\n", "\n");
            validator.validateXml(entity, message);
            assertEquals(new String(data).replaceAll("\\r\\n", "\n"), message.trim());
        } catch (JAXBException e) {
            fail("Unexpected exception during parsing data to xml");
        } catch (XmlValidationException e) {
            fail("The xml was expected to be valid: " + e.getMessage());
        } catch (RuntimeException e) {
            fail("Could not find the right xsd for the entity " + entity);
        }
    }


    @Test
    public void testXmlValidationException_TagsMissing() {
        XmlValidationException thrown = Assertions.assertThrows(XmlValidationException.class, () -> {
            InputStream inputStream = XmlParserAndValidatorTest.class.getResourceAsStream("/test_company_missing_tags.xml");
            assert inputStream != null;
            byte[] data = inputStream.readAllBytes();
            XmlValidator validator = new XmlValidator();
            validator.validateXml("company", data);
        });
    }

    @Test
    public void testXmlValidationException_StringInsteadOfNumberForPostcode() {
        XmlValidationException thrown = Assertions.assertThrows(XmlValidationException.class, () -> {
            InputStream inputStream = XmlParserAndValidatorTest.class.getResourceAsStream("/test_company_string_instead_of_number.xml");
            assert inputStream != null;
            byte[] data = inputStream.readAllBytes();
            XmlValidator validator = new XmlValidator();
            validator.validateXml("company", data);
        });
    }

    @Test
    public void testXmlValidationException_WrongProperties() {
        XmlValidationException thrown = Assertions.assertThrows(XmlValidationException.class, () -> {
            InputStream inputStream = XmlParserAndValidatorTest.class.getResourceAsStream("/test_company_w_properties_of_user.xml");
            assert inputStream != null;
            byte[] data = inputStream.readAllBytes();
            XmlValidator validator = new XmlValidator();
            validator.validateXml("company", data);
        });
    }

    @Test
    public void testXmlValidationException_UserXmlWhenExpectingCompany() {
        XmlValidationException thrown = Assertions.assertThrows(XmlValidationException.class, () -> {
            InputStream inputStream = XmlParserAndValidatorTest.class.getResourceAsStream("/test_user.xml");
            assert inputStream != null;
            byte[] data = inputStream.readAllBytes();
            XmlValidator validator = new XmlValidator();
            validator.validateXml("company", data);
        });
    }

//    @Test
//    public void testXmlValidationException() {
//        Assertions.assertThrows(JAXBException.class, () -> {
//            InputStream inputStream = XmlParserAndValidatorTest.class.getResourceAsStream("/test_company_missing_tags.xml");
//            assert inputStream != null;
//            byte[] data = inputStream.readAllBytes();
//            CompanyDto c = XmlParser.xmlToData(data, CompanyDto.class);
//        });
//    }
//    throws IOException, JAXBException {
//        InputStream inputStream = XmlParserAndValidatorTest.class.getResourceAsStream("/test_company_missing_tags.xml");
//        assert inputStream != null;
//        byte[] data = inputStream.readAllBytes();
//        CompanyDto c = XmlParser.xmlToData(data, CompanyDto.class);
//        assertThrows(JAXBException.class);
//    }

    private static void initUserValues() throws IOException {
        InputStream inputStream = XmlParserAndValidatorTest.class.getResourceAsStream("/test_user.xml");
        assert inputStream != null;
        userXmlBytes = inputStream.readAllBytes();

        userControlDto = new UserDto();
        User u = new User();
        Address a = new Address();
        a.setStreet("Nijverheidskaai");
        a.setNumber("170");
        a.setZip("1070");
        a.setCity("Anderlecht");
        a.setCountry("Belgium");

        u.setFirstname("Jan");
        u.setLastname("Janssens");
        u.setEmail("janj@mail.com");
        u.setAddress(a);
        u.setPhone("+32483640090");

        userControlDto.setSource("kassa");
        userControlDto.setSourceId(1);
        userControlDto.setUuid("123e4567-e89b-12d3-a456-426614174000");
        userControlDto.setAction("create");
        userControlDto.setProperties(u);
    }

    private static void initCompanyValues() throws IOException {
        InputStream inputStream = XmlParserAndValidatorTest.class.getResourceAsStream("/test_company.xml");
        assert inputStream != null;
        companyXmlBytes = inputStream.readAllBytes();

        companyControlDto = new CompanyDto();
        Company c = new Company();
        Address a = new Address();
        a.setStreet("Nijverheidskaai");
        a.setNumber("170");
        a.setZip("1070");
        a.setCity("Anderlecht");
        a.setCountry("Belgium");

        c.setName("EHB");
        c.setAddress(a);
        c.setEmail("info@ehb.be");
        c.setPhone("+32489310984");
        c.setTaxId("BE0255710113");

        companyControlDto.setSource("kassa");
        companyControlDto.setSourceId(2);
        companyControlDto.setUuid("123e4567-e89b-12d3-a456-426614174000");
        companyControlDto.setAction("update");
        companyControlDto.setProperties(c);

    }

    private static void initOrderValues() throws IOException {
        InputStream inputStream = XmlParserAndValidatorTest.class.getResourceAsStream("/test_order.xml");
        assert inputStream != null;
        orderXmlBytes = inputStream.readAllBytes();

        orderControlDto = new OrderDto();
        Order o = new Order();
        Identifiers i = new Identifiers();
        List<OrderLine> ol = Arrays.asList(
                new OrderLine() {{
                    setProductName("cola");
                    setUnityPrice(2.00);
                    setQuantity(2);
                }},
                new OrderLine() {{
                    setProductName("chips");
                    setUnityPrice(1.50);
                    setQuantity(1);
                }}
        );

        i.setSourceId("55");
        i.setUuid("66");

        o.setDate(LocalDateTime.ofEpochSecond(1652791974, 0, ZoneOffset.UTC));
        o.setTotal(6.00);
        o.setUserIds(i);
        o.setToInvoice(false);
        o.setOrderLines(ol);
        o.setPaid(false);

        orderControlDto.setSource("kassa");
        orderControlDto.setSourceId("55");
        orderControlDto.setUuid("123e4567-e89b-12d3-a456-426614174000");
        orderControlDto.setAction("create");
        orderControlDto.setProperties(o);
    }
}