#FROM openjdk:17
#COPY . /usr/src/myapp
#WORKDIR /usr/src/myapp
#CMD ["java", "-jar", "target/kassa-1.0-SNAPSHOT-jar-with-dependencies.jar"]

FROM maven:3.8.4-openjdk-17 AS build
COPY src /home/app/src
COPY pom.xml /home/app
RUN mvn -f /home/app/pom.xml clean package

FROM openjdk:17-alpine
COPY --from=build /home/app/target/kassa-jar-with-dependencies.jar /usr/local/lib/demo.jar
CMD ["java", "-jar", "/usr/local/lib/demo.jar"]